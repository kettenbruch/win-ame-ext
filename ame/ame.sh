#!/usr/bin/env bash
LC_ALL=C
LANG=C
export LC_ALL LANG
ameworkdir=$(dirname -- "$0")
windir=$(stat -c %m -- "$0")
echo "ameworkdir=$ameworkdir, windir=$windir"

function print_usage {
  echo "usage: [sudo] $0 [-b]"
  echo "-n: dry run (don't delete anything)"
  echo "-b: make a backup of files before deletion"
  echo "-s: leave files in WinSxS directory unchanged"
  echo "-k: exclude the index file from AME_Backup"
  echo "-g: Only generate Index and quit"
  echo "-v: be verbose"
  echo "-t: log each deleted file and its corresponding ame_term"
  echo "-h: show this usage message and quit"
  echo 
  echo "example: sudo $0 -p /dev/sda3 -b"
}

function copy_wallpaper {
  # delete generated (scaled) wallpapers, replace the sources, let windows regenerate them
  find "$windir/ProgramData/Microsoft/Windows/SystemData/S-1-5-18/ReadOnly" -type f -name "*.jpg" -delete
  find "$windir/Windows/Web/Wallpaper/Theme1" -type f ! -name "*.ini" -delete
  find "$windir/Windows/Web/Wallpaper/Windows" -type f ! -name "*.ini" -delete
  find "$windir/Windows/Web/Screen" -type f ! -name "*.ini" -delete
  find "$windir/Windows/Web/4K" -type f ! -name "*.ini" -delete
  for i in {1..5..1}; do
    # replace default desktop backgrounds
    cp "$1/Desktop/img$i.jpg" "$windir/Windows/Web/Wallpaper/Theme1/img$i.jpg"
    # replace default lockscreens
    cp "$1/Desktop/img$i.jpg" "$windir/Windows/Web/Screen/img$((i + 100)).jpg"
  done
  echo "searching WinSxS"
  bgdirs=$(find "$windir/Windows/WinSxS" -maxdepth 1 -type d -name '*backgrounds-client*')
  find "$bgdirs/" -type f -name "*.png" -delete
  for i in ${bgdirs}; do # replace default lockscreen
    cp -fa "$1"/img*.jpg "$i"
  done
}

create_archive() {
  if tar -czf "$1" "$2"; then
    rm -r "$2"
  else
    echo "tar returned with errors, skipping deletion of directory $2"
  fi
}

unalias cp 2>/dev/null
BACKUP_DIR="AME_Backup_$(date +%Y-%m-%dT%H-%M)"
DO_BACKUP=0
DRY_RUN=0
VERBOSE=0
KEEP_INDEX=0
KEEP_SXS=0
ONLY_GEN_IDX=0
ASSOCIATE_TERM=0
OTHER_OPTIONS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -b|--backup)    DO_BACKUP=1; shift 1;;
    -g|--generate)  ONLY_GEN_IDX=1; shift 1;;
    -k|--keepindex) KEEP_INDEX=1; shift 1;;
    -s|--keepsxs)   KEEP_SXS=1; shift 1;;
    -t|--associate) ASSOCIATE_TERM=1; shift 1;;
    --backupdir)    BACKUP_DIR="$2"; shift 2;;
    -n|--dry-run)   DRY_RUN=1; shift 1;;
    -v|--verbose)   VERBOSE=1; shift 1;;
    -h|--help)      print_usage;;
    -*)             echo "Invalid argument '$1'"; print_usage; exit 1;;
    *)              OTHER_OPTIONS+=("$1"); shift 1;;
  esac
done
if [ ${#OTHER_OPTIONS[@]} -gt 0 ]; then print_usage; exit 1; fi

# include debugging/testing functions
source ../tmp/debug.sh 2>/dev/null
debug_args 2>/dev/null
debug_mnt 2>/dev/null

# see https://docs.microsoft.com/en-us/windows/application-management/system-apps-windows-client-os
# 1527c705-839a-4832-9118-54d4Bd6a0c89 Microsoft.Windows.FilePicker
# c5e2524a-ea46-4f67-841f-6a9465d9d515 universal file explorer
# E2A4F912-2574-4A75-9BB0-0D023378592B App Resolver UX
editAppxDB() {
  if ! command -v sqlite3 &>/dev/null; then
    echo "sqlite3 could not be found, please install it"
    echo "e.g. by running 'sudo apt install sqlite3'"
    read -p "to skip editing the database, press enter"
    return 1
  else
    local srd="$windir/ProgramData/Microsoft/Windows/AppRepository/StateRepository-Machine.srd"
    stat "$srd" >/dev/null || return 1
    # backup and drop a trigger which interferes with IsInbox changes
    cp "$srd" "$windir/StateRepository-Machine.srd"
    local trigbak
    trigbak=$(sqlite3 -batch "$srd" "SELECT sql FROM sqlite_master WHERE type ='trigger' AND name IS 'TRG_AFTER_UPDATE_Package_SRJournal';")
    if [ -z "$trigbak" ]; then
      echo "Error: trigger backup variable is empty, skipping!"
      echo "trigbak was set with: sqlite3 -batch \"$srd\" \"SELECT sql FROM sqlite_master WHERE type = 'trigger' AND name IS 'TRG_AFTER_UPDATE_Package_SRJournal';\""
      return 1
    else
      sqlite3 -batch "$srd" "DROP TRIGGER TRG_AFTER_UPDATE_Package_SRJournal;"
      local res=$?
      sqlite3 -batch "$srd" <<'END_UPDATE'
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%AAD%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%AccountsControl%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%BioEnrollment%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%CBSPreview%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%CallingShell%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%CapturePicker%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%ChxApp%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%CloudExperience%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%ContentDelivery%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%Dev%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%ECApp%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%GpuEject%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%NarratorQuickStart%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%OOBE%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%ParentalControls%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%PeopleExperience%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%PinningConfirmation%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%Search%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%SecHealth%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%WebView%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE '%Xbox%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE 'F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE%';
      UPDATE main.Package SET IsInbox = 0 WHERE PackageFullName LIKE 'c5e2524a-ea46-4f67-841f-6a9465d9d515%';
END_UPDATE
      res=$((res | $?))
      # restore the trigger
      sqlite3 -batch "$srd" -line "$trigbak"
      if ((!(res | $?))); then
        echo "success"
        printf "modified configuration:\n\n"
        sqlite3 -batch "$srd" "SELECT IsInbox,PackageFullName FROM main.Package ORDER BY IsInbox DESC;"
      else
          >&2 echo "failed to unlock Appx Packages for removal, restoring"
          cp -f "$windir/StateRepository-Machine.srd" "$srd"
      fi
      return $res
    fi
  fi
}

gen_idx() {
if [ -f ame_idx.txt ]; then
    echo "using pre-generated index"
  else
    echo -n "generating index, this can take some time"
    find "$windir" -printf '%i!"%p"\n' >ame_idx.tmp || echo "'find' failed !"
    mv ame_idx.tmp ame_idx.txt
    echo "done"
  fi
  printf "\nsize of index: %s\n" "$(du -h ame_idx.txt)"
}

if [[ $ONLY_GEN_IDX -eq 1 ]]; then gen_idx; echo "generated index, quitting"; exit 0; fi

echo -n "editing Appx StateRepositoryMachine..."
if editAppxDB; then
  echo "Appx packages unlocked for removal."
fi

# Copy Wallpapers
if [[ -d "$ameworkdir/Wallpaper" ]] && [[ -n $(ls "$ameworkdir/Wallpaper") ]]; then
  echo "copying Wallpapers"
  if copy_wallpaper "$ameworkdir/Wallpaper"; then echo "success"; else echo "failed"; fi
fi

# transition to windows partition
cp "$ameworkdir/util/ame_terms.txt" "$windir/ame_terms.txt"
if ! cd "$windir"; then
  >&2 echo "can't cd to $windir"
  exit 1
fi
rm -r .Trash* 2>/dev/null

printf "\nStarting AME process\n"

gen_idx

# append service names to terms list
truncate -s 0 ame_list.txt ame_list_ext.txt ame_list_cleaned.txt remove.sh check.sh
#experimental
#sed -i $'s/\r$//g' svcterms.txt
#cat svcterms.txt >> ame_terms.txt

printf "\nsearching for terms and listing matches\n"
readarray -t Terms <ame_terms.txt
# Experimental search terms: /netbios certprop /sync ContentDelivery
# Alternative: processing all terms at once is faster, but won't report the number of matches per term.
# grep -f ./util/ame_terms.txt ame_idx.txt >>ame_list.txt
if [[ $ASSOCIATE_TERM -eq 1 ]]; then
    for i in "${Terms[@]}"; do
      echo "$i" >>ame_assoc.txt
      echo "$i: $(grep -Fi "$i" ame_idx.txt | tee --append >(wc -l) ame_assoc.txt >>ame_list.txt)"
    done
  else
    for i in "${Terms[@]}"; do
      echo "$i: $(grep -Fi "$i" ame_idx.txt | tee >(wc -l) >>ame_list.txt)"
    done
fi
if [ "$(stat -c %s ame_list.txt)" -eq 0 ]; then
  echo "unable to find anything matching 'Terms', quitting."
  exit 1
fi
# add all hardlinks
grep -Fw "$(cut -d! -f1 ame_list.txt | sort | uniq | sed 's#\n#!\n#g')" ame_idx.txt | cut -d! -f2- >ame_list_ext.txt

# matching and removing "false positives" from the deletion list, see "neededFiles" in ../doc
neededPaths=('$Recycle.Bin' "AppRepository" "BthTelemetry.dll" "FileMaps" "MSRAWImage" "Telemetry.Common.dll" "Windows.Gaming.Input.dll" "Windows.CloudStore" "xboxgip.sys")
if [ ${KEEP_SXS} -eq 1 ]; then neededPaths+=("WinSxS"); fi
grep -Fiv "$(printf '%s\n' "${neededPaths[@]}")" ame_list_ext.txt > ame_list_cleaned.txt

# Specific components
# Localization
neededKBDs='KBD(US|GR|SG)\.DLL'
neededLanguages='de-DE|en-US'

echo "Removing Keyboard DLLs"
grep -Eo '\./Windows/(System32|SysWOW64)/KBD[[:alnum:]]*\.DLL$' ame_idx.txt | grep -Ev "$neededKBDs" >>ame_list_cleaned.txt

echo "Removing unused Languages"
grep -Eo '\./Windows/(System32|SysWOW64)/[a-z]{2}-[A-Z]{2}$' ame_idx.txt | grep -Ev "$neededLanguages" >>ame_list_cleaned.txt

# Convert filenames from Windows to valid unix paths e.g. C:\Windows\System32\icsvext.dll -> /mnt/Windows/System32/icsvext.dll
sed -i $'s/\r//' svcdlls.txt
sed -e "s#[A-Za-z]:#$windir#" -e 's#\\#/#g' svcdlls.txt | sort | uniq -i 1>>ame_list_cleaned.txt

# removal script
echo '#!/bin/bash' | tee remove.sh check.sh
echo "echo 'Removing files'" >>remove.sh
echo "echo 'Files remaining:'" >>check.sh
awk '{print "rm -rf " $0}' ame_list_cleaned.txt >>remove.sh
awk '{print "test -e " $0 "&& echo " $0}' ame_list_cleaned.txt >>check.sh
echo 'echo done' | tee -a remove.sh check.sh
chmod +x remove.sh check.sh

# clear backup directory contents
rm -r "$BACKUP_DIR" 2>/dev/null
mkdir "$BACKUP_DIR"

# backup process
if [ ${DO_BACKUP} -eq 1 ]; then
  echo "Backing up files"
  rsync -a --files-from=ame_list_cleaned.txt / "$BACKUP_DIR/"
fi

if [ ${DRY_RUN} -eq 0 ]; then
  ./remove.sh
  sync
  ./check.sh
fi

echo "Cleaning up"
declare -a FilesToMove=("svcdlls.txt" "svcterms.txt" "remove.sh" "check.sh" "ame_list.txt" "ame_list_ext.txt" "ame_list_cleaned.txt" "ame_terms.txt" "$windir/StateRepository-Machine.srd")
if [ ${KEEP_INDEX} -eq 0 ]; then FilesToMove+=("ame_idx.txt"); fi
if [ ${DO_BACKUP} -eq 1 ]; then FilesToMove+=("restore.sh"); fi
for i in "${FilesToMove[@]}"; do
  mv "$i" "$BACKUP_DIR/"
done

# pack and check
echo "packing"
if [ ${DRY_RUN} -eq 0 ]; then
  create_archive "${BACKUP_DIR}.tar.gz" "${BACKUP_DIR}"
  echo "$(basename "$0") finished"
else
  create_archive "${BACKUP_DIR}_DRY_RUN.tar.gz" "${BACKUP_DIR}"
  echo "DRY RUN finished"
fi
debug_backup 2>/dev/null
sync

read -p "Reboot into Windows now [y/N]? " bRebootNow
case $bRebootNow in
[Yy]*) reboot ;;
*) exit ;;
esac
