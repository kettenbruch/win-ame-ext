$ErrorActionPreference = "Inquire"
# This script adds simplewall (https://github.com/henrypp/simplewall) as addition to the windows firewall and cleans up existing rules 

# Enable Filtering Platform Logging, for debugging 
# cmd /c "auditpol /set /subcategory:`"{0CCE9226-69AE-11D9-BED3-505054503030}`" /success:disable /failure:enable"

# the blocked applications can be seen with the command below (Source: https://serverfault.com/questions/316428/does-windows-firewall-have-the-ability-to-log-which-exe-is-blocked)
# Get-EventLog security -newest 10 -InstanceId 5157 -Message *Destination* | select @{Name="message";Expression={ $_.ReplacementStrings[1] }}
# disable logging: cmd /c "auditpol /set /subcategory:`"{0CCE9226-69AE-11D9-BED3-505054503030}`" /success:disable /failure:disable"

# Windows Firewall: 
# blocking outbound would be very effective but is too much effort to maintain manually, as windows firewall doesn't prompt for creating outbound rules.
Set-NetFirewallProfile -DefaultInboundAction Block -NotifyOnListen True -LogAllowed False -LogBlocked False -LogIgnored False

echo "Uninstalling the local transactions coordinator (msdtc). removing the firewall rules"
Uninstall-Dtc -Confirm:0 -ErrorAction Ignore
Get-NetFirewallRule | where {$_.Name -like "MSDTC*"} | Remove-NetFirewallRule -ErrorAction SilentlyContinue

echo "Deleting FirewallRules..."
Get-NetFirewallRule | where {`
     $_.DisplayName -like "*Search*"`
 -or $_.DisplayName -like "*Peer*"`
 -or $_.DisplayName -like "Desktop App Web Viewer"`
 -or $_.DisplayName -like "*Lock Screen*"`
 -or $_.DisplayName -like "*Windows Security*"`
 -or $_.DisplayName -like "*?ms-resource*"`
 -or $_.DisplayName -like "*SNMP*"`
 -or $_.DisplayName -like "*account*"`
 -or $_.DisplayName -like "*WFD*"`
 -or $_.DisplayName -like "*Wi-Fi Direct*"`
 -or $_.DisplayName -like "*Proximity sharing*"`
 -or $_.DisplayName -like "*Remote*"`
 -or $_.DisplayName -like "*Windows Collaboration*"`
 -or $_.DisplayName -like "*Defender SmartScreen*"`
 -or $_.DisplayName -like "Microsoft Edge*"`
 -or $_.Name -like "AllJoyn*"`
 -or $_.Name -like "WirelessDisplay*"`
 -or $_.Name -like "*Troubleshooting*"`
 -or $_.Name -like "*SSTP-IN-TCP*"`
 -or $_.Name -like "*ProvSvc*"`
 -or $_.Name -like "TPMVSCMGR*"`
 -or $_.Name -like "vm*"`
 -or $_.Name -like "DeliveryOptimization*"`
 -or $_.Name -like "*Telemetry*"`
 -or $_.Name -like "*Netlogon*"`
 -or $_.Name -like "*iscsi*"`
 -or $_.Name -like "NETDIS*"`
 -or $_.Name -like "*Enrollment*"`
 -or $_.Name -like "DIAL-Protocol*"`
 -or $_.Name -like "*OmaDmClient*" `
} | Remove-NetFirewallRule -ErrorAction SilentlyContinue


# installing simplewall 
$si = "${Env:ProgramFiles}\simplewall"
if (Test-Path "$si") {echo "found simplewall, skipping"}
else {
  echo "installing simplewall..."
  ( mkdir "$si" ) 1>$null
  mv .\util\firewall\simplewall.exe "$si"
  mv .\util\firewall\simplewall.ini "$si"
  mv .\util\firewall\profile.xml "$si"
  # add simplewall to path
  [Environment]::SetEnvironmentVariable("Path", ${env:Path} + ";$si", "Machine")
}
echo "starting simplewall..."
Start-Process -FilePath "$si\simplewall.exe" -Verb RunAs -ArgumentList "-install -silent"
