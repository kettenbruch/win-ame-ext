﻿# Nvidia driver debloating script,
# dependencies: 7z
$PSDefaultParameterValues['Out-File:Encoding'] = 'utf8' # setup.cfg needs utf8
$ErrorActionPreference = "Inquire"

# lookup 7zip path from registry
if ((Test-path "HKLM:\SOFTWARE\7-Zip\") -eq $true) {
  $7zpath = $($(Get-ItemProperty -path "HKLM:\SOFTWARE\7-Zip\" -Name Path).Path + "7z.exe")
  if ((Test-Path $7zpath) -ne $true) {echo "failed to find 7z, registry value: $7zpath" ; exit 1}
}

function extract7z
{
  iex "&`"$7zpath`" x `"$drvpack`" -o`"$work`" -y"
}

if($args.Count -ne 1)
{
  echo "no executable specified, guessing ..."
  $guesses = @(`
  $($(ls -File).Name -match '^[0-9]+.*(win).*\.exe$'),`
  $($(ls -File).Name -match '^[0-9]{3}\.[0-9]{2}\-\w*\-(win).*\.exe$'),`
  $($(ls -File).Name -match '^[0-9]{3}\.[0-9]{2}\-\w*\-(win).*\-(32|64)(bit).*\.exe$'),`
  $($(ls -File).Name -match '^[0-9]{3}\.[0-9]{2}\-\w*\-(win).*\-(32|64)(bit).*(whql).*\.exe$') )
  for($p=0; $p -lt $guesses.Length; $p=$p+1)
  {
    switch ($guesses[$p].Count)
    {
      {$_ -eq 1} {$drvpack = $($guesses[$p]);echo "found $drvpack"; $leave=$true}
      {$_ -eq 0} {echo "found no driver file, please specify a path as first and only argument."; exit 1}
      default
      {
        echo "no unique match in pattern $p..."
        if($($p+1) -eq $guesses.Length -and $guesses[$p].Count -ne 1) {echo "Error: found $($guesses[$p].Count) executables, please specify the executable as argument. aborting"; exit}
      }
    }
    if($leave){Remove-Variable leave;break}
  }
}
else
{
  echo "got 1 argument"
  $drvpack = $args[0]
}

$work = "$($(ls $drvpack).Basename)" # or e.g. ".\slim-drivers"

echo "extracting $drvpack to $work"
if (Test-Path -Path $work)
{
  $overwrite = Read-Host "extracted folder already exists, replace [y/N] ?"
  if ($overwrite -eq 'y') { rm -r $work }
  else {echo "quitting" ; exit}
}

extract7z $drvpack $work
# Removing folders and files, except terms (whitelist) below
# nvcamera = Ansel
ls $work |`
where{$_.name -ne "Display.Driver"`
 -and $_.name -ne "NVI2"`
 -and $_.name -ne "PhysX"`
 -and $_.name -ne "nvcamera"`
 -and $_.name -ne "ListDevices.txt"`
 -and $_.name -ne "setup.cfg"`
 -and $_.name -ne "EULA.txt"`
 -and $_.name -ne "setup.exe" } | rm -r -force

# removing lines from setup.cfg
# this might scramble non-ascii strings in setup.cfg
cat "$work\setup.cfg" | Where-Object {
  $_ -notlike '*EulaHtmlFile*' -and`
  $_ -notlike '*FunctionalConsentFile*' -and`
  $_ -notlike '*PrivacyPolicyFile*' } | Out-File "$work\setup2.cfg"
mv -force "$work\setup2.cfg" "$work\setup.cfg"

$launch = Read-Host "do you want to launch the installer [y/n]?"
if ($launch -eq 'y')
{
  iex "& $work\setup.exe"
}

echo "script finished"
exit
# Troubleshooting:
# If the Installer exits with error "Module not found", check if the 'NVI2' folder and 'NVI2/nvi2.dll' exist.
# Also validate that all files in the '<manifest>' section at the end of 'setup.cfg' exist.
