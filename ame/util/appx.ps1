# exclude some packages with:
# $a = Get-AppxPackage -AllUsers | where {$_.Name -notlike "*VCLibs*" -or ... }

$a = Get-AppxPackage -AllUsers
$n0 = ($a | measure).count

foreach ($p in $a) { Try {Remove-AppxPackage $p -AllUsers -ErrorVariable +RemoveErrors} Catch {} }

$n1 = ($(Get-AppxPackage -AllUsers) | measure).count

echo "  :: Removed $($n0 - $n1) Packages, $($RemoveErrors.Count) Errors."

echo "  :: Remaining Packages:"
Get-AppxPackage -AllUsers | select IsBundle,name
