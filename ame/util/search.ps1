param (
  [String]$TermFile,
  [String]$IndexFile
)

$MatchesFile = "C:\matches.txt"
echo "TermFile = $TermFile"
echo "IndexFile = $IndexFile"
# $TermFile="C:\ame_terms.txt"; $Indexfile="C:\demo.txt"; $MatchesFile = "C:\matches.txt"

#if($IndexFile.Contains("attr")) { echo "fixing up" ; #foreach ($i in $Index) { $i = $i.substring(21) } ; #[IO.File]::WriteAllLines($IndexFile, $Index)}

$Index = New-Object -TypeName System.IO.StreamReader -ArgumentList ($IndexFile, [System.Text.Encoding]::ASCII, $false);
$TermReader = New-Object -TypeName System.IO.StreamReader -ArgumentList ($TermFile, [System.Text.Encoding]::ASCII, $false);
[String]$Terms=$TermReader.ReadLine();
while(!$TermReader.EndOfStream){$Terms += '|' + $TermReader.ReadLine()}
$TG =  New-Object -TypeName System.Text.RegularExpressions.Regex -ArgumentList ($Terms, 521);  # 512 = IgnoreCase | Compiled | CultureInvariant
[String[]]$NeededTerms = "FileMaps", "MSRAW", "Telemetry.Common.dll", "Windows.Gaming.Input.dll", "xboxgip.sys"
[String]$needed = [Regex]::Escape($NeededTerms[0])
for($i = 1; $i -lt $NeededTerms.length;$i++){$needed += '|' + [Regex]::Escape($NeededTerms[$i])}
$NG =  New-Object -TypeName System.Text.RegularExpressions.Regex -ArgumentList ($needed, 521);
$MatchesWriter = New-Object -TypeName System.IO.StreamWriter -ArgumentList ($MatchesFile, $false, [System.Text.Encoding]::ASCII);

while (!$Index.EndOfStream)
{
    $IndexPath = $Index.ReadLine().substring(21);
    $IndexLine = [Regex]::Escape($IndexPath);
    if($NG.ismatch($IndexLine)) {echo "skipping"; continue}
    elseif($TG.ismatch($IndexLine)){ $MatchesWriter.WriteLine($IndexPath) }
    #(Select-String -Pattern $Term -InputObject $Index -SimpleMatch).Line >> matches.txt
}

$TermReader.Dispose()
$MatchesWriter.Dispose()
$Index.Dispose()

