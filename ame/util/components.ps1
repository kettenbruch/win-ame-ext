# Windows Component Remover
param (
  [String]$mode="Online",
  [String]$mountpath,
  [Boolean]$keepmedia=1
)

#$ErrorActionPreference = "Inquire"
$w6p = "$PsScriptRoot\w6rt.exe"
# w6rt is from https://msfn.org/board/topic/152688-win6x_registry_tweak/ hosted at https://github.com/shiitake/win6x_registry_tweak

if ($mode -eq "Offline") {
  if (Test-Path $mountpath){
    echo "Offline usage in '$mountpath'"
    $d = "/n /p $mountpath"
  }
  else {echo "Invalid path '$mountpath' specified" ; exit }
}
elseif ($mode -eq "Online") {
  echo "Assuming Online usage as no '-mode' parameter was specified"
  $d = "/o"
}
else { echo "unknown mode $mode" ; exit }
$t0 = (Get-Date).ToUniversalTime() # time of start
echo "Removing components with flag $d"

# note that package-names are case-sensitive

iex "& $w6p $d /l" # list all packages

# Message Queueing - internal components seem to rely on this (Digital Signatures Panel on Files)
#iex "& $w6p $d /c `"Microsoft-Windows-msmq`" /r"
#iex "& $w6p $d /c `"Microsoft-Windows-MSMQ`" /r"
#iex "& $w6p $d /c `"Microsoft-Windows-COM-MSMQ`" /r"
#iex "& $w6p $d /c `"MSMQ-Driver-Package`" /r"

#iex "& $w6p $d /c `"Microsoft-Windows-UpdateTargeting`" /r" # needed by CBS ?
#iex "& $w6p $d /c `"Microsoft-Windows-Branding`" /r" # branding folder - basebrd, shellbrd
#iex "& $w6p $d /c `"Microsoft-Windows-Security-SPP`" /r" # Licensing component, needed on install for entering productkey
#iex "& $w6p $d /c `"Microsoft-Windows-BootEnvironment-Dvd`" /r" # bcd, efisys.bin, used to create os images ?

#iex "& $w6p $d /c `"Microsoft-Windows-ClientForNFS`" /r" # Network File System Client -> Linux
#iex "& $w6p $d /c `"Microsoft-Windows-NFS`" /r"

#iex "& $w6p $d /c `"Microsoft-Windows-Dedup-ChunkLibrary`" /r" # Data deduplication,
#iex "& $w6p $d /c `"Microsoft-Windows-Management-SecureAssessment`" /r"
#iex "& $w6p $d /c `"Networking-MPSSVC`" /r" # Part of WindowsFirewall

if(-not $keepmedia)
{
    echo "removing MediaPack"
    iex "& $w6p $d /c `"Microsoft-Windows-Media`" /r"
    iex "& $w6p $d /c `"Microsoft-Windows-WMPNetworkSharingService`" /r"
    iex "& $w6p $d /c `"Microsoft-Windows-WindowsMediaPlayer`" /r"
}
iex "& $w6p $d /c `"Containers-`" /r"
iex "& $w6p $d /c `"HyperV`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Rec`" /r" # RecDisk and RecoveryDrive "Recovery Media Creator"
iex "& $w6p $d /c `"Microsoft-Windows-ScreenSavers`" /r"  #3d
iex "& $w6p $d /c `"LanguageFeatures-WordBreaking`" /r"
iex "& $w6p $d /c `"Microsoft-Hyper-V`" /r"
iex "& $w6p $d /c `"Microsoft-IoTUAP`" /r"
iex "& $w6p $d /c `"Microsoft-OneCore-Containers-Guest`" /r"
iex "& $w6p $d /c `"Microsoft-OneCore-DeviceUpdateCenter`" /r"
iex "& $w6p $d /c `"Microsoft-OneCore-Helium`" /r" # ?
iex "& $w6p $d /c `"Microsoft-OneCore-UtilityVM`" /r"
iex "& $w6p $d /c `"Microsoft-OneCore-UtilityVm`" /r"
iex "& $w6p $d /c `"Microsoft-OneCore-VirtualizationBasedSecurity`" /r" # seems to be used by vm only
iex "& $w6p $d /c `"Microsoft-OneCore-WindowsIoT`" /r"
iex "& $w6p $d /c `"Microsoft-Onecore-Identity`" /r"
iex "& $w6p $d /c `"Microsoft-Shielded-VM`" /r"
iex "& $w6p $d /c `"Microsoft-UtilityVM-Containers`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-ApiSetSchemaExtension-HyperV`" /r" # ?
iex "& $w6p $d /c `"Microsoft-Windows-AppManagement-UEV`" /r" # User Experience Virtualization
iex "& $w6p $d /c `"Microsoft-Windows-Bio`" /r" # Bio enrollment - Windows Hello Setup ?
# Simplified, Traditional, Japanese and Korean Fonts -(Hans, Hant, Jpan, Kore)
iex "& $w6p $d /c `"Microsoft-OneCore-Fonts-DesktopFonts`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Browser`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Client-AssignedAccess`" /r" # Kiosk mode
iex "& $w6p $d /c `"Microsoft-Windows-Client-EmbeddedExp`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Client-ShellLauncher`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-ConfigCI`" /r" # Configurable Code Integrity
iex "& $w6p $d /c `"Microsoft-Windows-DataCenterBridging`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-DeviceGuard-GPEXT`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-DirectPlay`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-DirectoryServices-ADAM`" /r" # AD App Module
iex "& $w6p $d /c `"Microsoft-Windows-DirectoryServices-adam`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Embedded`" /r" # embeddedmode, CE, IoT, ?
iex "& $w6p $d /c `"Microsoft-Windows-EnterpriseClientSync`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-FCI-Client`" /r" # File Classification Infrastructure
iex "& $w6p $d /c `"Microsoft-Windows-FodMetadata`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-GroupPolicy`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-HVSI`" /r" # Defender Application Guard ?
iex "& $w6p $d /c `"Microsoft-Windows-Hello`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Help`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Holographic-Desktop`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-HyperV`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Hyphenation-Dictionaries`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-IIS-WebServer`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Identity`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Internet-Browser`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-InternetExplorer`" /r" #  removal has no effect, will stay listed
iex "& $w6p $d /c `"Microsoft-Windows-LanguageFeatures-Handwriting`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-LanguageFeatures-OCR`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-LanguageFeatures-Speech`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-LanguageFeatures-TextToSpeech`" /r" # no 'Basic'
iex "& $w6p $d /c `"Microsoft-Windows-Legacy-Components`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Lxss`" /r" # Linux Subsystem
iex "& $w6p $d /c `"Microsoft-Windows-MobilePC`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Media-Streaming`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-MultiPoint-Connector`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-NewTabPageHost`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Not-Supported-On-LTSB`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-OfflineFiles`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-OneCore-Containers-Opt`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-OneCore-Containers`" /r" # vm containers
iex "& $w6p $d /c `"Microsoft-Windows-OneDrive`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-PAW`" /r" # Privileged Access Workstation
iex "& $w6p $d /c `"Microsoft-Windows-PeerDist`" /r" # Peer Distribution
iex "& $w6p $d /c `"Microsoft-Windows-PhotoBasic`" /r" # Photos App ?
iex "& $w6p $d /c `"Microsoft-Windows-PowerShell-ISE`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Printing-InternetPrinting`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Printing-LP`" /r" # Line Printers for UNIX ?
iex "& $w6p $d /c `"Microsoft-Windows-Printing-WFS`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Printing-XPSServices`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Printing-Xps`" /r" # xps file format
iex "& $w6p $d /c `"Microsoft-Windows-ProjFS`" /r" # Projected Filesystem
iex "& $w6p $d /c `"Microsoft-Windows-QuickAssist`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-RDC`" /r" # Remote Desktop
iex "& $w6p $d /c `"Microsoft-Windows-RemoteAssistance`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-RemoteFX`" /r" # Case sensitive ...
iex "& $w6p $d /c `"Microsoft-Windows-Remotefx`" /r" # Video Card interface for VM ?
iex "& $w6p $d /c `"Microsoft-Windows-SMB1`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-SearchEngine`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-SenseClient`" /r" # antimalware ?
iex "& $w6p $d /c `"Microsoft-Windows-SimpleTCP`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-SmbDirect`" /r" # Remote DMA for SMB
iex "& $w6p $d /c `"Microsoft-Windows-Spelling`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-StepsRecorder`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-TFTP`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-TabShellExperience`" /r" # uwp feature ?
iex "& $w6p $d /c `"Microsoft-Windows-TabletPCMath`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Telnet`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-TerminalServices`" /r" # RemoteDesktopProtocol Implementation
iex "& $w6p $d /c `"Microsoft-Windows-TextPrediction`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-Virtualization`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-WinOcr`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-msmq-adintegration`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-msmq-dcomproxy`" /r"
iex "& $w6p $d /c `"Microsoft-Windows-msmq-multicast`" /r"
iex "& $w6p $d /c `"MultiPoint`" /r" # Special Devices
iex "& $w6p $d /c `"RemoteDesktopServices`" /r"
iex "& $w6p $d /c `"Server-Help-Package`" /r"
iex "& $w6p $d /c `"Windows-Defender`" /r"
iex "& $w6p $d /c `"WindowsSearchEngineSKU`" /r"

# keep the Packages.txt from the beginning and generate a new one for comparison, re-hide the Components.
mv -Force Packages.txt Packages_pre.txt
iex "& $w6p $d /l"
mv -Force Packages.txt Packages_post.txt
iex "& $w6p $d /h /l"
echo ""
$t1 = (Get-Date).ToUniversalTime()
$a = $(cat Packages_pre.txt | Measure-Object -Line).Lines
$b = $(cat Packages_post.txt | Measure-Object -Line).Lines

echo "Removed $($a - $b) Components, $b remaining"

# w6rt creates backups of the SOFTWARE hive, if not suppressed with '-n'
# See https://git.ameliorated.info/lucid/win6x_registry_tweak/src/commit/531e5a815afb855591b50f2bd03ff4eadb5b0d2a/Program.cs#L179
if (Test-Path $PSScriptRoot\SOFTWAREBKP)
{
  echo "found leftover SOFTWAREBKP, removing"
  rm $PSScriptRoot\SOFTWAREBKP
}

echo "components.ps1 done"
echo "script execution time was:"
$($t1-$t0)
