# Windows AME Extended ISO preparation script,

# the Windows adk is required: https://docs.microsoft.com/en-us/windows-hardware/get-started/adk-install
# Get the latest Cumulative Update from 'https://catalog.update.microsoft.com',
# extract the msu package (eg. with 7z) and move the .cab file to $updates

#$ErrorActionPreference = "Inquire"
$date = Get-Date -UFormat '+%Y-%m-%d'

# the script is intended to run unattended, the parameters are hardcoded here.
$dp = "${Env:ProgramFiles(x86)}\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\DISM"
$mp = "$Env:SystemDrive\mnt"
$ip = "$Env:SystemDrive\iso"
`"$ip\sources\install.wim`" = `"$ip\sources\install.wim`"

$updates = "$PSScriptRoot\..\build\updates"
$OrgIso = "$PSScriptRoot\..\build\Win10_22H2_English_x64.iso"
$IsoOut = "$PSScriptRoot\..\build\ame$date.iso"
$KeepMedia = 0 # use a edition with (KeepMedia=1) or without MediaFeaturePack
$UnattendFile = "$PSScriptRoot\..\build\unattend.xml"

$LogFile = "$PSScriptRoot\iso-setup$date.log"
$lvl = "INFO" # comment out to treat every Write-Log call as breakpoint (debugging)

Function Write-Log {
  [CmdletBinding()]
  Param(
    [Parameter(Mandatory = 0)] [ValidateSet("INFO", "WARN", "ERROR", "FATAL", "DEBUG")] $Level,
    [Parameter(Mandatory = 1)] [String] $Message,
    [Parameter(Mandatory = 0)] [String] $LogFile
  )
  $entry = "$(Get-Date -Format o) $Level $Message"
  If (Test-Path "$LogFile") { Add-Content $LogFile -Value "$entry" ; Write-Output "$entry" }
  Else { Write-Output $entry }
}

# cleanup before starting
rm -recurse "$Env:SystemDrive\Windows\Logs\DISM\*"
rm -recurse "$Env:SystemDrive\Windows\Logs\CBS\*"
mkdir -Force $mp
mkdir -Force $ip
rm "$mp/*"
rm "$ip/*"
echo "" > $LogFile

# extract OS orgISO
if( (Get-ChildItem $ip | Measure-Object).Count -eq 0)
{ iex "& `"${Env:ProgramFiles}\7-Zip\7z.exe`" x -y -o`"$ip`" `"$OrgIso`""
} else { echo "''$ip' is not empty, using contents" }

Write-Log -Level $lvl -Message "removing unused editions from install.wim" $LogFile
for ($i = 11; $i -gt 2; $i--)
{
  Write-Log -Level $lvl -Message "Deleting Image index:$i" $LogFile
  iex "&`"$dp\dism.exe`" /Delete-Image /ImageFile:`"$ip\sources\install.wim`" /index:$i"
}

if($KeepMedia) { # 1 - Core, 2 - CoreN, ...
  iex "&`"$dp\dism.exe`" /Delete-Image /ImageFile:`"$ip\sources\install.wim`" /index:2"
} else {
  iex "&`"$dp\dism.exe`" /Delete-Image /ImageFile:`"$ip\sources\install.wim`" /index:1"
}

Write-Log -Level $lvl -Message "Remaining indices:" $LogFile
iex "&`"$dp\dism.exe`" /get-WimInfo /WimFile:`"$ip\sources\install.wim`"" | Tee-Object -FilePath $LogFile

Write-Log -Level $lvl -Message "Mounting remaining index" $LogFile
iex "&`"$dp\dism.exe`" /mount-image /ImageFile:`"$ip\sources\install.wim`" /index:1 /MountDir:$mp"

Write-Log -Level $lvl -Message "adding updates" $LogFile
ls "$updates" -Filter *.cab | foreach { $_.FullName } `
| Foreach-Object { echo "adding $_..." ; iex "&`"$dp\dism.exe`" /Image:$mp /Add-Package /PackagePath:$_" }

Write-Log -Level $lvl -Message "Starting full component store cleanup" $LogFile
iex "&`"$dp\dism.exe`" /Image:$mp /Cleanup-Image /StartComponentCleanup /ResetBase"

Write-Log -Level $lvl -Message "Committing changes" $LogFile
iex "&`"$dp\dism.exe`" /Commit-Wim /MountDir:$mp"

Write-Log -Level $lvl -Message "Update done" $LogFile

Write-Log -Level $lvl -Message "Entering cleanup" $LogFile

Write-Log -Level $lvl -Message "Removing Features" $LogFile
Get-WindowsOptionalFeature -Path $mp `
| where { $_.State -eq "Enabled"`
 -and $_.FeatureName -NotLike "*PrintToPDF*"`
 -and $_.FeatureName -NotLike "Printing-Foundation-Features*"`
 -and $_.FeatureName -NotLike "MicrosoftWindowsPowershellV2*"`
 -and $_.FeatureName -NotLike "MediaPlayback*"`
 -and $_.FeatureName -NotLike "WindowsMedia*" }`
| Disable-WindowsOptionalFeature -LogLevel "WarningsInfo"
Get-WindowsOptionalFeature -Path $mp | where { $_.State -match "Disabled" } | `
foreach { `
  $_ = $_.FeatureName; iex "&`"$dp\dism.exe`" /Image:$mp /Disable-Feature /FeatureName:$_ /Remove" `
}
# note that since windows 10 /remove doesn't remove the payload, previously they were listed as "Disabled with Payload Removed"
Write-Log -Level $lvl -Message "Remaining Features" $LogFile
Get-WindowsOptionalFeature -Path $mp | where { $_.State -eq "Enabled" } | Tee-Object -FilePath $LogFile

Write-Log -Level $lvl -Message "Removing Capabilities" $LogFile
Get-WindowsCapability -Path $mp `
| where { $_.State -eq "Installed"`
 -and $_.Name -NotLike "Language.Basic~~~en-US*"`
 -and $_.Name -NotLike "OpenSSH.Client*"`
 -and $_.Name -NotLike "Microsoft.Windows.Notepad*"`
 -and $_.Name -NotLike "Microsoft.Windows.MSPaint*"`
 -and $_.Name -NotLike "Microsoft.WindowsMediaPlayer*" }`
| Remove-WindowsCapability -Path $mp -LogLevel "WarningsInfo"
# -and $_.Name -NotLike "Windows.Client.ShellComponents*"` # Feature Experience Pack
Write-Log -Level $lvl -Message "Removed Capabilities" $LogFile
Get-WindowsCapability -Path $mp | where { $_.State -eq "Installed" } | Tee-Object -FilePath $LogFile

Write-Log -Level $lvl -Message "Removing Provisioned Appx Packages" $LogFile

# Microsoft.MSPaint AppxPackage is Paint3D
Get-AppxProvisionedPackage -Path $mp `
| where { $_.DisplayName -NotLike "Microsoft.VCLibs*"`
 -and $_.DisplayName -NotLike "Microsoft.HEIF*"`
 -and $_.DisplayName -NotLike "Microsoft.WebMedia*" }`
| Remove-AppxProvisionedPackage

Write-Log -Level $lvl -Message "Remaining AppxProvisionedPackages:" $LogFile
Get-AppxProvisionedPackage -Path $mp | select DisplayName | Tee-Object -FilePath $LogFile

Write-Log -Level $lvl -Message "Cleanup section done, committing" $LogFile
iex "&`"$dp\dism.exe`" /Commit-Wim /MountDir:$mp"

Write-Log -Level $lvl -Message "Removing Components with w6rt, be patient: this can take a long time" $LogFile
iex "& .\util\components.ps1 -mode Offline -MountPath $mp -KeepMedia $KeepMedia"

Write-Log -Level $lvl -Message "Cleaning the Component Store..." $LogFile
iex "&`"$dp\dism.exe`" /Image:$mp /Cleanup-Image /StartComponentCleanup /ResetBase"

Write-Log -Level $lvl -Message "w6rt section finished" $LogFile

If (Test-Path "$UnattendFile") { cp $UnattendFile "$ip\sources\`$OEM`$\`$`$\Panther" }

Write-Log -Level $lvl -Message "Cleaning up and unmounting" $LogFile

iex "&`"$dp\dism.exe`" /Image:$mp /Optimize-ProvisionedAppxPackages"
iex "&`"$dp\dism.exe`" /unmount-image /MountDir:$mp /commit"

iex "&`"$dp\dism.exe`" /Cleanup-MountPoints"

Write-Log -Level $lvl -Message "Exporting Compressed Wim" $LogFile
iex "&`"$dp\dism.exe`" /Export-Wim /CheckIntegrity /Compress:Max /SourceImageFile:`"$ip\sources\install.wim`" /SourceIndex:1 /DestinationImageFile:`"$ip\sources\install_temporary.wim`""
mv "$ip\sources\install_temporary.wim" "$ip\sources\install.wim"

Write-Log -Level $lvl -Message "Building ISO" $LogFile
pushd "$dp\..\oscdimg"
iex "& `".\oscdimg.exe`" -m -o -u2 -b`".\efisys.bin`" $ip $IsoOut"
popd

Write-Log -Level $lvl -Message "script finished" $LogFile
