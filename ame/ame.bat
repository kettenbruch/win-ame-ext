:: Windows AME Extended, tested on Win10 22H2
:: The user created during initial setup is considered the final user.
:: When other users are needed, create them after running sections [1-3].
:: The newly created user will inherit settings from the default user which will be created during the first run.


@echo off
pushd "%~dp0"
set "ameversion=0.5.b"

echo   :: Checking for Administrator Elevation...
openfiles >nul 2>&1 || (
  PowerShell -NoP -C "Start-Process -Verb RunAs -FilePath cmd -Args '/C CD /D %CD% & %0'"
  exit
)

goto menu
:menu
echo.
echo   :: Windows AME Extended Batch Script %ameversion%
echo      This is a overview of sections this script uses:
echo.
echo   d : Install NetFx3 (dotnet3.5) from install media
echo   1 : Run Pre-Amelioration
echo   2 : Run Post-Amelioration
echo   3 : User Permissions
echo   r : Restart System
echo   c : Enter custom batch label to start execution
echo.
echo   :: Default Actions:
echo   :: start at 1, reboot to linux, execute ame.sh
echo   :: proceed with 3 and 4
echo.
echo   :: Type a 'number' and press ENTER
echo   :: Type 'exit' to quit
echo.

set /P menu=
  if "%menu%"=="d" goto dotnet
  if "%menu%"=="c" goto choosegoto
  if "%menu%"=="1" goto preame1
  if "%menu%"=="2" goto postame
  if "%menu%"=="3" goto user
  if "%menu%"=="r" goto reboot
  if "%menu%"=="exit" exit /b else (
  echo.
  echo :: Incorrect Input Entered
  echo.
  echo    Please type a 'number' or 'exit'
  echo    Press any key to return to the menu...
  echo.
  pause >nul
  goto menu
)

:choosegoto
set /P label=goto :
goto %label%

:dotnet
:: TODO: choco
:: DotNet 3.5 Installation from install media
echo   :: Installing .NET 3.5 for Windows 10
echo.
echo      Windows 10 normally opts to download this runtime via Windows Update.
echo      However, it can be installed with the original installation media.
echo      .NET 3.5 is necessary for certain programs and games to function.
echo.
echo   :: Please mount the Windows 10 installation media and specify a drive letter.
echo.
echo   :: Type a 'drive letter' e.g. D: and press ENTER
echo   :: Type 'exit' to return to the menu
echo.
set /P drive=
if "%drive%"=="exit" goto menu
dism /online /enable-feature /featurename:NetFX3 /All /Source:%drive%\sources\sxs /LimitAccess
goto menu

:preame1
echo.

echo   :: Stopping Services
:: using 'net' waits for the service to stop, 'sc' doesn't
sc stop wuauserv 1>nul
sc stop wlidsvc 1>nul
sc stop BITS 1>nul

echo.
echo   :: Disabling Data Logging Services
taskkill /f /im explorer.exe

echo   :: Deleting Scheduled Tasks
:: display with Get-ScheduledTask | where {$_.State -eq "Running" -or $_.State -eq "Ready"} | select Taskpath,Taskname
:: schtasks /delete /TN "\Microsoft\Windows\TextServicesFramework\MsCtfMonitor" /f :: needed for entering text in uwp apps
schtasks /delete /TN "\MicrosoftEdgeUpdateTaskMachineCore" /f
schtasks /delete /TN "\MicrosoftEdgeUpdateTaskMachineUA" /f
schtasks /delete /TN "\Microsoft\Windows\Active Directory Rights Management Services Client\AD RMS Rights Policy Template Management (Automated)" /f
schtasks /delete /TN "\Microsoft\Windows\Active Directory Rights Management Services Client\AD RMS Rights Policy Template Management (Manual)" /f
schtasks /delete /TN "\Microsoft\Windows\AppID\EDP Policy Manager" /f
schtasks /delete /TN "\Microsoft\Windows\AppID\PolicyConverter" /f
schtasks /delete /TN "\Microsoft\Windows\AppID\VerifiedPublisherCertStoreCheck" /f
schtasks /delete /TN "\Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /f
schtasks /delete /TN "\Microsoft\Windows\Application Experience\PcaPatchDbTask" /f
schtasks /delete /TN "\Microsoft\Windows\Application Experience\ProgramDataUpdater" /f
schtasks /delete /TN "\Microsoft\Windows\Application Experience\StartupAppTask" /f
schtasks /delete /TN "\Microsoft\Windows\ApplicationData\DsSvcCleanup" /f
schtasks /delete /TN "\Microsoft\Windows\Autochk\Proxy" /f
schtasks /delete /TN "\Microsoft\Windows\BitLocker\BitLocker Encrypt All Drives" /f
schtasks /delete /TN "\Microsoft\Windows\BitLocker\BitLocker MDM policy Refresh" /f
schtasks /delete /TN "\Microsoft\Windows\BrokerInfrastructure\BgTaskRegistrationMaintenanceTask" /f
schtasks /delete /TN "\Microsoft\Windows\CloudExperienceHost\CreateObjectTask" /f
schtasks /delete /TN "\Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /f
schtasks /delete /TN "\Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /f
schtasks /delete /TN "\Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /f
schtasks /delete /TN "\Microsoft\Windows\DUSM\dusmtask" /f
schtasks /delete /TN "\Microsoft\Windows\Defrag\ScheduledDefrag" /f
schtasks /delete /TN "\Microsoft\Windows\Device Information\Device User" /f
schtasks /delete /TN "\Microsoft\Windows\Device Information\Device" /f
schtasks /delete /TN "\Microsoft\Windows\Device Setup\Metadata Refresh" /f
schtasks /delete /TN "\Microsoft\Windows\Diagnosis\RecommendedTroubleshootingScanner" /f
schtasks /delete /TN "\Microsoft\Windows\Diagnosis\Scheduled" /f
schtasks /delete /TN "\Microsoft\Windows\DiskCleanup\SilentCleanup" /f
schtasks /delete /TN "\Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" /f
schtasks /delete /TN "\Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticResolver" /f
schtasks /delete /TN "\Microsoft\Windows\DiskFootprint\Diagnostics" /f
schtasks /delete /TN "\Microsoft\Windows\DiskFootprint\StorageSense" /f
schtasks /delete /TN "\Microsoft\Windows\Feedback\siuf\DmClientOnScenarioDownload" /f
schtasks /delete /TN "\Microsoft\Windows\Feedback\siuf\dmclient" /f
schtasks /delete /TN "\Microsoft\Windows\FileHistory\File History (maintenance mode)" /f
schtasks /delete /TN "\Microsoft\Windows\Flighting\FeatureConfig\ReconcileFeatures" /f
schtasks /delete /TN "\Microsoft\Windows\Flighting\FeatureConfig\UsageDataFlushing" /f
schtasks /delete /TN "\Microsoft\Windows\Flighting\FeatureConfig\UsageDataReporting" /f
schtasks /delete /TN "\Microsoft\Windows\Flighting\OneSettings\RefreshCache" /f
schtasks /delete /TN "\Microsoft\Windows\HelloFace\FODCleanupTask" /f
schtasks /delete /TN "\Microsoft\Windows\Input\LocalUserSyncDataAvailable" /f
schtasks /delete /TN "\Microsoft\Windows\Input\MouseSyncDataAvailable" /f
schtasks /delete /TN "\Microsoft\Windows\Input\PenSyncDataAvailable" /f
schtasks /delete /TN "\Microsoft\Windows\Input\TouchpadSyncDataAvailable" /f
schtasks /delete /TN "\Microsoft\Windows\InstallService\ScanForUpdates" /f
schtasks /delete /TN "\Microsoft\Windows\InstallService\ScanForUpdatesAsUser" /f
schtasks /delete /TN "\Microsoft\Windows\InstallService\SmartRetry" /f
schtasks /delete /TN "\Microsoft\Windows\InstallService\WakeUpAndContinueUpdates" /f
schtasks /delete /TN "\Microsoft\Windows\InstallService\WakeUpAndScanForUpdates" /f
schtasks /delete /TN "\Microsoft\Windows\International\Synchronize Language Settings" /f
schtasks /delete /TN "\Microsoft\Windows\Location\Notifications" /f
schtasks /delete /TN "\Microsoft\Windows\Location\WindowsActionDialog" /f
schtasks /delete /TN "\Microsoft\Windows\Maintenance\WinSAT" /f
schtasks /delete /TN "\Microsoft\Windows\Management\Provisioning\Cellular" /f
schtasks /delete /TN "\Microsoft\Windows\Management\Provisioning\Logon" /f
schtasks /delete /TN "\Microsoft\Windows\Management\Provisioning\Retry" /f
schtasks /delete /TN "\Microsoft\Windows\Management\Provisioning\RunOnReboot" /f
schtasks /delete /TN "\Microsoft\Windows\Maps\MapsToastTask" /f
schtasks /delete /TN "\Microsoft\Windows\Maps\MapsUpdateTask" /f
schtasks /delete /TN "\Microsoft\Windows\MemoryDiagnostic\ProcessMemoryDiagnosticEvents" /f
schtasks /delete /TN "\Microsoft\Windows\MemoryDiagnostic\RunFullMemoryDiagnostic" /f
schtasks /delete /TN "\Microsoft\Windows\Mobile Broadband Accounts\MNO Metadata Parser" /f
schtasks /delete /TN "\Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /f
schtasks /delete /TN "\Microsoft\Windows\Printing\EduPrintProv" /f
schtasks /delete /TN "\Microsoft\Windows\PushToInstall\LoginCheck" /f
schtasks /delete /TN "\Microsoft\Windows\PushToInstall\Registration" /f
schtasks /delete /TN "\Microsoft\Windows\Ras\MobilityManager" /f
schtasks /delete /TN "\Microsoft\Windows\Registry\RegIdleBackup" /f
schtasks /delete /TN "\Microsoft\Windows\Servicing\StartComponentCleanup" /f
schtasks /delete /TN "\Microsoft\Windows\SettingSync\BackgroundUploadTask" /f
schtasks /delete /TN "\Microsoft\Windows\SettingSync\NetworkStateChangeTask" /f
schtasks /delete /TN "\Microsoft\Windows\Setup\SetupCleanupTask" /f
schtasks /delete /TN "\Microsoft\Windows\SharedPC\Account Cleanup" /f
schtasks /delete /TN "\Microsoft\Windows\Shell\FamilySafetyMonitor" /f
schtasks /delete /TN "\Microsoft\Windows\Shell\FamilySafetyRefreshTask" /f
schtasks /delete /TN "\Microsoft\Windows\SoftwareProtectionPlatform\SvcRestartTaskLogon" /f
schtasks /delete /TN "\Microsoft\Windows\SoftwareProtectionPlatform\SvcRestartTaskNetwork" /f
schtasks /delete /TN "\Microsoft\Windows\Speech\SpeechModelDownloadTask" /f
schtasks /delete /TN "\Microsoft\Windows\StateRepository\MaintenanceTasks" /f
schtasks /delete /TN "\Microsoft\Windows\Storage Tiers Management\Storage Tiers Management Initialization" /f
schtasks /delete /TN "\Microsoft\Windows\Storage Tiers Management\Storage Tiers Optimization" /f
schtasks /delete /TN "\Microsoft\Windows\Sysmain\HybridDriveCachePrepopulate" /f
schtasks /delete /TN "\Microsoft\Windows\Sysmain\HybridDriveCacheRebalance" /f
schtasks /delete /TN "\Microsoft\Windows\Sysmain\ResPriStaticDbSync" /f
schtasks /delete /TN "\Microsoft\Windows\Sysmain\WsSwapAssessmentTask" /f
schtasks /delete /TN "\Microsoft\Windows\SystemRestore\SR" /f
schtasks /delete /TN "\Microsoft\Windows\TPM\Tpm-HASCertRetr" /f
schtasks /delete /TN "\Microsoft\Windows\TPM\Tpm-Maintenance" /f
schtasks /delete /TN "\Microsoft\Windows\Time Synchronization\ForceSynchronizeTime" /f
schtasks /delete /TN "\Microsoft\Windows\Time Synchronization\SynchronizeTime" /f
schtasks /delete /TN "\Microsoft\Windows\Time Zone\SynchronizeTimeZone" /f
schtasks /delete /TN "\Microsoft\Windows\UNP\RunUpdateNotificationMgr" /f
schtasks /delete /TN "\Microsoft\Windows\UPnP\UPnPHostConfig" /f
schtasks /delete /TN "\Microsoft\Windows\User Profile Service\HiveUploadTask" /f
schtasks /delete /TN "\Microsoft\Windows\WDI\ResolutionHost" /f
schtasks /delete /TN "\Microsoft\Windows\WOF\Wim-Hash-Management" /f
schtasks /delete /TN "\Microsoft\Windows\WOF\Wim-Hash-Validation" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Error Reporting\QueueReporting" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Filtering Platform\BfeOnServiceStartTypeChange" /f
schtasks /delete /TN "\Microsoft\Windows\Wininet\CacheTask" /f
schtasks /delete /TN "\Microsoft\Windows\Workplace Join\Automatic-Device-Join" /f
schtasks /delete /TN "\Microsoft\Windows\Workplace Join\Device-Sync" /f
schtasks /delete /TN "\Microsoft\Windows\Workplace Join\Recovery-Check" /f
schtasks /delete /TN "\Microsoft\Windows\WwanSvc\NotificationTask" /f
schtasks /delete /TN "\Microsoft\Windows\WwanSvc\OobeDiscovery" /f
schtasks /delete /TN "\Microsoft\XblGameSave\XblGameSaveTask" /f
schtasks /Change /TN "\Microsoft\Windows\ExploitGuard\ExploitGuard MDM policy Refresh" /disable
schtasks /Change /TN "\Microsoft\Windows\License Manager\TempSignedLicenseExchange" /disable
schtasks /Change /TN "\Microsoft\Windows\Management\Autopilot\DetectHardwareChange" /disable
schtasks /Change /TN "\Microsoft\Windows\Management\Autopilot\RemediateHardwareChange" /disable
schtasks /Change /TN "\Microsoft\Windows\PI\Secure-Boot-Update" /disable
schtasks /Change /TN "\Microsoft\Windows\PI\Sqm-Tasks" /disable
schtasks /Change /TN "\Microsoft\Windows\RecoveryEnvironment\VerifyWinRE" /disable
schtasks /Change /TN "\Microsoft\Windows\Shell\ThemesSyncedImageDownload" /disable
schtasks /Change /TN "\Microsoft\Windows\WlanSvc\CDSSync" /disable
schtasks /change /TN "\Microsoft\Windows\Clip\License Validation" /tr "%SystemRoot%\system32\rundll32.exe"
schtasks /change /TN "\Microsoft\Windows\Subscription\EnableLicenseAcquisition" /tr "%SystemRoot%\system32\rundll32.exe"
schtasks /change /TN "\Microsoft\Windows\Subscription\LicenseAcquisition" /tr "%SystemRoot%\system32\rundll32.exe"

echo (may not exist)
schtasks /delete /TN "\MicrosoftEdgeUpdateBrowserReplacementTask" /f
schtasks /delete /TN "\Microsoft\Windows\Shell\IndexerAutomaticMaintenance" /f
schtasks /delete /TN "\Microsoft\Windows\RemoteAssistance\RemoteAssistanceTask" /f
schtasks /delete /TN "\Microsoft\Windows\RetailDemo\CleanupOfflineContent" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Cache Maintenance" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Cleanup" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Scheduled Scan" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Verification" /f
schtasks /change /TN "\Microsoft\Windows\SoftwareProtectionPlatform\SvcRestartTask" /tr "%SystemRoot%\system32\rundll32.exe"
schtasks /change /TN "\Microsoft\Windows\SoftwareProtectionPlatform\SvcRestartTaskLogon" /tr "%SystemRoot%\system32\rundll32.exe"
schtasks /change /TN "\Microsoft\Windows\SoftwareProtectionPlatform\SvcRestartTaskNetwork" /tr "%SystemRoot%\system32\rundll32.exe"
taskkill /f /im sppextcomobj.exe
.\util\nsudolc.exe -U:T -wait -UseCurrentConsole cmd /c copy /Y "%windir%\System32\rundll32.exe" "%windir%\System32\sppextcomobj.exe"
.\util\nsudolc.exe -U:T -wait -UseCurrentConsole cmd /c sc stop sppsvc
.\util\nsudolc.exe -U:T -wait -UseCurrentConsole cmd /c sc config sppsvc start= disabled
.\util\nsudolc.exe -U:T -wait -UseCurrentConsole cmd /c sc config clipsvc start= disabled
:: Create a pair of Scheduled Tasks which disable network until the user is logged in
:: see https://superuser.com/questions/1555206/how-to-block-network-leaks-during-boot-time-on-windows
schtasks /create /xml "util\firewall\netadpt0.xml" /tn "\netadpt0" /ru "SYSTEM"
schtasks /create /xml "util\firewall\netadpt1.xml" /tn "\netadpt1" /ru "SYSTEM"

:: Set Defragment Optimization Schedule Frequency to monthly
reg add "HKLM\SOFTWARE\Microsoft\Dfrg\TaskSettings" /v "TaskFrequency" /t REG_DWORD /d 4 /f
:: Alternative: disable automatic defragmentation
:: schtasks /change /TN "\Microsoft\Windows\Defrag\ScheduledDefrag" /disable

echo Removing tasks with S-1-5-18 (Local System) privileges
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\Report Policies" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\Schedule Maintenance Work" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\Schedule Scan Static Task" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\Schedule Scan" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\Schedule Wake To Work" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\Schedule Work" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\USO_UxBroker" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\UpdateOrchestrator\UpdateModelTask" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\WaaSMedic\PerformRemediation" /f
:: EDP - data loss protection for enterprise
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\EDP\EDP Inaccessible Credentials Task" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\EDP\StorageCardEncryption Task" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\EDP\EDP App Launch Task" /f
.\util\nsudolc.exe -U:S -wait -UseCurrentConsole schtasks /delete /TN "\Microsoft\Windows\EDP\EDP Auth Task" /f
:: Deleting empty task directories, rmdir skips all nonempty folders
:: for /f "delims=" %%d in ('dir /s /b /ad %windir%\System32\Tasks ^| sort /r') do rmdir "%%d"
:: Alternative: robocopy moves only non-empty directories and deletes the rest
robocopy /nfl /ndl /njh /s /move "%windir%\system32\tasks" "%windir%\system32\tasks"

echo   :: Disabling Computer Restore
PowerShell -NoP -C "Disable-ComputerRestore -Drive '%SystemDrive%\'"
vssadmin delete shadows /all
:netshares

echo   :: Deleting all Network Shares
for /f "usebackq" %%s in (`PowerShell -NoP -C "$(get-smbshare).name"`) do net share %%s /delete
sc stop lanmanserver

echo "" >%ProgramData%\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl
:: ContentEvaluation - Disables Smartscreen
:: WaitToKillAppTimeOut - Decrease Shutdown time
:: SkipQuickStart - Skip Narrator Quick start
:: optionally add the following:
:: reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Attachments" /v "SaveZoneInformation" /t REG_DWORD /d 1 /f
echo   :: Setting Policies
for %%p in ( HKLM HKCU HKU\.DEFAULT ) do (
  reg add "%%p\AppEvents\Schemes" /ve /d ".None" /f 2>nul
  reg add "%%p\Control Panel\Desktop" /v "WaitToKillAppTimeOut" /t REG_SZ /d 2000 /f 2>nul
  reg add "%%p\Control Panel\International\User Profile" /v "HttpAcceptLanguageOptOut" /t REG_DWORD /d 1 /f 2>nul
  reg add "%%p\Control Panel\Mouse" /v "MouseSpeed" /t REG_SZ /d "0" /f
  reg add "%%p\SOFTWARE\Microsoft\Input\Settings" /v "InsightsEnabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Input\TIPC" /v "Enabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Narrator\QuickStart" /v "SkipQuickStart" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Siuf\Rules" /v "NumberOfSIUFInPeriod" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo" /v "Enabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost" /v "ContentEvaluation" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "ShowFrequent" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "ShowRecent" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "Start_TrackDocs" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" /v "AppCaptureEnabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "AllowOnlineTips" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "ClearRecentDocsOnExit" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoInstrumentation" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoInternetOpenWith" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoOnlinePrintsWizard" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoPublishingWizard" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoRecentDocsHistory" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoWebServices" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\PushNotifications" /v "ToastEnabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "BackgroundAppGlobalToggle" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\SearchSettings" /v "IsDeviceSearchHistoryEnabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "DisableNotificationCenter" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "DisableSearchBoxSuggestions" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "DisableSearchHistory" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "DisableThumbsDBOnNetworkFolders" /t REG_DWORD /d 1 /f
  reg add "%%p\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "NoSearchInternetInStartMenu" /t REG_DWORD /d 1 /f
  reg add "%%p\SYSTEM\GameConfigStore" /v "GameDVR_Enabled" /t REG_DWORD /d 0 /f
  :: notification 'Choose backup options'
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Notifications\Settings\Windows.SystemToast.BackupReminder" /v "Enabled" /t REG_DWORD /d 0 /f
  echo. & echo denying ConsentStore entries in %%p...
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "Start_TrackProgs" /t REG_DWORD /d 0 /f
  reg delete "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications" /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications" /v "GlobalUserDisabled" /t REG_DWORD /d 1 /f
  for %%i in ( documentsLibrary picturesLibrary videosLibrary broadFileSystemAccess ) do (
    reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\CapabilityAccessManager\ConsentStore\%%i" /v "Value" /t REG_SZ /d "Deny" /f
  )
)
echo   :: Setting Policies - HKLM
reg add "HKLM\SOFTWARE\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots" /v "value" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" /v "value" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows Script Host\Settings" /v "Enabled" /t REG_SZ /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config" /v "DownloadMode" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers" /v "DisableAutoplay" /t REG_DWORD /d 1 /f
:: Disables SmartScreen
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "SmartScreenEnabled" /t REG_SZ /d "Off" /f
:: Remove Recycle Bin from Desktop
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v "{645FF040-5081-101B-9F08-00AA002F954E}" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\ImmersiveShell" /v "UseActionCenterExperience" /t REG_DWORD /d 0 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control\WMI\AutoLogger\AutoLogger-Diagtrack-Listener" /v "Start" /t REG_DWORD /d 0 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control\WMI\AutoLogger\SQMLogger" /v "Start" /t REG_DWORD /d 0 /f
:: Set Hardware Time to UTC
reg add "HKLM\SYSTEM\CurrentControlSet\Control\TimeZoneInformation" /v "RealTimeIsUniversal" /t REG_DWORD /d 1 /f

:: Setting Group Policies
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\CurrentVersion\Software Protection Platform" /v "NoGenTicket" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo" /v "DisabledByGroupPolicy" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\DataCollection" /v "AllowTelemetry" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\EnhancedStorageDevices" /v "TCGSecurityActivationDisabled" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\OneDrive" /v "DisableFileSyncNGSC" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers" /v "AuthenticodeEnabled" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\SettingSync" /v "DisableSettingSync" /t REG_DWORD /d 2 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\SettingSync" /v "DisableSettingSyncUserOverride" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting" /v "DontSendAdditionalData" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\WindowsStore" /v "AutoDownload" /t REG_DWORD /d 4 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\WindowsStore" /v "DisableOSUpgrade" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\WindowsStore" /v "RemoveWindowsStore" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Biometrics" /v "Enabled" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\FileHistory" /v "Disabled" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\WMDRM" /v "DisableOnline" /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows\LanmanWorkstation" /v "AllowInsecureGuestAuth" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableLocation" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableLocationScripting" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\LocationAndSensors" /v "DisableSensors" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\GameDVR" /v "AllowGameDVR" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\FindMyDevice" /v "AllowFindMyDevice" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AllowCloudSearch" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AllowCortana" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AllowIndexingEncryptedStoresOrItems" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AllowSearchToUseLocation" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "AlwaysUseAutoLangDetection" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "ConnectedSearchUseWeb" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "ConnectedSearchUseWebOverMeteredConnections" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Windows Search" /v "DisableWebSearch" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "HideSCAHealth" /t REG_DWORD /d 1 /f
:: https://www.stigviewer.com/stig/windows_10/2021-08-18/finding/V-220828
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoAutorun" /t REG_DWORD /d 1 /f
:: Prevent creation of Microsoft Accounts
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "NoConnectedUser" /t REG_DWORD /d 1 /f
:: SystemSettings cleanup - List of commands: https://winaero.com/ms-settings-Cs-in-windows-10/
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "SettingsPageVisibility" /t REG_SZ /d "showonly:display;sound;notifications;quiethours;powersleep;batterysaver;nightlight;tabletmode;multitasking;about;bluetooth;printers;mousetouchpad;devices-touchpad;devices-touch;typing;autoplay;usb;network-status;network-cellular;network-wifi;network-wifisettings;network-ethernet;network-dialup;network-vpn;network-airplanemode;network-mobilehotspot;network-proxy;personalization-background;colors;lockscreen;fonts;personalization-start;taskbar;appsfeatures;defaultapps;startupapps;dateandtime;regionlanguage-jpnime;regionlanguage-quickime;regionlanguage;regionformatting;keyboard-advanced;keyboard;easeofaccess-display;easeofaccess-colorfilter;easeofaccess-audio;easeofaccess-highcontrast;easeofaccess-keyboard;easeofaccess-mouse;easeofaccess-mousepointer" /f

reg add "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "dontdisplaylastusername" /t REG_DWORD /d 1 /f
:: Enable verbose sign-in / logout messages
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "VerboseStatus" /t REG_DWORD /d 1 /f
:: Disabling Data Collection
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection" /v "AllowTelemetry" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Policies\DataCollection" /v "AllowTelemetry" /t REG_DWORD /d 0 /f
:: Turns off Windows blocking installation of files downloaded from the Internet
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Attachments" /v "SaveZoneInformation" /t REG_DWORD /d 1 /f
:: Disable Timeline
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "EnableActivityFeed" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "PublishUserActivities" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "UploadUserActivities" /t REG_DWORD /d 0 /f
:: Sign-in
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\CredUI" /v "DisablePasswordReveal" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Personalization" /v "NoLockScreen" /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Personalization" /v "NoLockScreenCamera" /t REG_DWORD /d 1 /f
:: Windows Ink Workspace
reg add "HKLM\SOFTWARE\Policies\Microsoft\WindowsInkWorkspace" /v "AllowWindowsInkWorkspace" /t REG_DWORD /d 0 /f
:: System Restore
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore" /v "DisableConfig" /t "REG_DWORD" /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore" /v "DisableSR" /t "REG_DWORD" /d 1 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore" /v "DisableConfig" /t "REG_DWORD" /d 1 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore" /v "DisableSR" /t "REG_DWORD" /d 1 /f

echo   :: Applying Fixes for High Severity STIG Findings
:: https://www.stigviewer.com/stig/windows_10/
echo | set /p="V-220929 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v "RestrictAnonymousSAM" /t REG_DWORD /d 1 /f
echo | set /p="V-220930 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v "RestrictAnonymous" /t REG_DWORD /d 1 /f
echo | set /p="V-220931 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v "EveryoneIncludesAnonymous" /t REG_DWORD /d 0 /f
echo | set /p="V-220932 " & reg add "HKLM\SYSTEM\CurrentControlSet\Services\LanManServer\Parameters" /v "RestrictnullSessAccess" /t REG_DWORD /d 1 /f
echo | set /p="V-220937 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v "NoLMHash" /t REG_DWORD /d 1 /f
echo | set /p="V-220938 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v "LmCompatibilityLevel" /t REG_DWORD /d 5 /f
echo | set /p="V-220857 " & reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer" /v "AlwaysInstallElevated" /t REG_DWORD /d 0 /f
echo | set /p="V-220823 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance" /v "fAllowToGetHelp" /t REG_DWORD /d 0 /f
echo | set /p="V-220823 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance" /v "fAllowFullControl" /t REG_DWORD /d 0 /f
echo | set /p="V-220823 " & reg add "HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance" /v "fEnableChatControl" /t REG_DWORD /d 0 /f
echo | set /p="V-220829 " & reg add "HKLM\SOFTWARE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoDriveTypeAutoRun" /t REG_DWORD /d "0xff" /f
echo | set /p="V-220827 " & reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "NoAutoplayfornonVolume" /t REG_DWORD /d 1 /f

echo   :: Denying Permissions for UWP Apps
for %%i in ( AccessAccountInfo AccessGazeInput AccessCallHistory AccessContacts AccessEmail AccessLocation AccessMessaging AccessMotion AccessNotifications AccessTasks AccessCalendar AccessCamera AccessMicrophone AccessTrustedDevices AccessBackgroundSpatialPerception AccessRadios AccessPhone ActivateWithVoice ActivateWithVoiceAboveLock GetDiagnosticInfo SyncWithDevices RunInBackground ) do (
  reg add "HKLM\Software\Policies\Microsoft\Windows\AppPrivacy" /v "LetApps%%i" /t REG_DWORD /d 2 /f
)

reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Device Metadata" /f
:: Removing SecHealthSystray from Autorun
reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "SecurityHealth" /f
:: TODO Deleting Autoruns
reg delete "HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components\{9459C573-B17A-45AE-9F64-1857B5D58CEE}" /f

:: Decrease shutdown time
reg add "HKLM\SYSTEM\CurrentControlSet\Control" /v "WaitToKillServiceTimeout" /t REG_SZ /d 2000 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control" /v "HungAppTimeout" /t REG_SZ /d 2000 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control" /v "AutoEndTasks" /t REG_SZ /d 1 /f

:: setting TaskManager preferences, annoyingly this has to be done by importing a reg_binary
reg import "util\taskmgr.reg"

:: disable NetBios for all existing interfaces
PowerShell -NoP -C "ls HKLM:\SYSTEM\CurrentControlSet\Services\netbt\Parameters\interfaces | foreach { Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\netbt\Parameters\interfaces\$($_.pschildname) -name NetBiosOptions -value 2}"
:: change sound scheme to "no sounds" - part 2 - https://superuser.com/questions/1300539/change-sound-scheme-in-windows-via-windows-registry
PowerShell -NoP -C "ls -Path 'HKCU:\AppEvents\Schemes\Apps' | ls | ls | Where-Object {$_.PSChildName -eq '.Current'} | Set-ItemProperty -Name '(Default)' -Value ''"

:: Set PowerShell Font Size to 12
for %%p in ( HKCU HKU\.DEFAULT ) do (
  for %%a in ( System32 SysWOW64 ) do (
    reg add "%%p\Console\%SystemRoot%_%%a_WindowsPowerShell_v1.0_powershell.exe" /v "FaceName" /t REG_SZ /d "Lucida Console" /f
    reg add "%%p\Console\%SystemRoot%_%%a_WindowsPowerShell_v1.0_powershell.exe" /v "FontFamily" /t REG_DWORD /d 0x00000036 /f
    reg add "%%p\Console\%SystemRoot%_%%a_WindowsPowerShell_v1.0_powershell.exe" /v "FontWeight" /t REG_DWORD /d 0x00000190 /f
    reg add "%%p\Console\%SystemRoot%_%%a_WindowsPowerShell_v1.0_powershell.exe" /v "FontSize" /t REG_DWORD /d 0x000c0000 /f
  )
)

echo Starting Services cleanup
:: Reference: https://github.com/MicrosoftDocs/windowsserverdocs/blob/master/WindowsServerDocs/security/windows-services/security-guidelines-for-disabling-system-services-in-windows-server.md
:: Reference: doc/services.csv
:: For a exhaustive list of system services use Get-Service | select ServiceName,DisplayName
:: List of running Services: Get-Service | where {$_.Status -eq "Running"} | select ServiceName,DisplayName

set "SvcDisable=AarSvc Beep bindflt CaptureService DsmSvc fdPHost fhsvc FileInfo FileCrypt GpuEnergyDrv ikeext InstallService iphlpsvc kdnic ndiscap PcaSvc PrintNotify Rmsvc seclogon sfloppy spooler SstpSvc sysmain tokenbroker TrkWks UnistoreSvc UserDataSvc WalletService wbengine wcncsvc wpnservice wephostsvc wmiApSrv xboxgip"


set "SvcDelete=ALG AsyncMac AxInstSV BcastDvrUserService BITS BTAGService BthAvctpSvc bttflt CDPSvc CDPUserSvc CldFlt ConsentUxUserSvc DevQueryBroker DevicePickerUserSvc dfsc diagnosticshub.standardcollector.service diagsvc DiagTrack DispBrokerDesktopSvc dmwappushservice dssvc DusmSvc edgeupdate edgeupdatem FDResPub flpydisk FrameServer gencounter hvcrash icssvc lanmanserver lfsvc lmhosts MapsBroker MessagingService MicrosoftEdgeElevationService Microsoft_Bluetooth_AvrcpTransport mstee NaturalAuthentication NcdAutoSetup ndiswan ndiswanlegacy ndproxy ndu netbt Netlogon peauth perceptionsimulation Phonesvc PimIndexMaintenanceSvc p2pimsvc p2psvc pnrpsvc PNRPAutoReg PushToInstall rdpbus RDPDR RdpVideoMiniport RemoteAccess RemoteRegistry RetailDemo RpcLocator sdrsvc SEMgrSvc sensordataservice sensorservice sensrsvc SessionEnv SharedAccess SharedRealitySvc ShellHWDetection shpamsvc SNMPTRAP spaceparser ssdpsrv stexstor StiSvc StorSvc storflt swprv TabletInputService TapiSrv Telemetry terminpt termservice Themes TieringEngineService tpm TroubleshootingSvc TsUsbFlt TsUsbGD tzautoupdate UmRdpService upnphost usbcir UsoSvc vmbus vmbushid vmgid vpci vss wanarp wanarpv6 WbioSrvc wcifs wcnfs wercplsupport WerSvc WFDSconMgrSvc WiaRpc WindowsTrustedRT WindowsTrustedRTProxy WinHttpAutoProxySvc winmad winrm winverbs wisvc wlidsvc wpcmonsvc WPDBusEnum ws2ifsl WSearch wuauserv WwanSvc XblAuthManager XblGameSave XboxGipSvc XboxNetApiSvc"

:: Services owned by System and TrustedInstaller respectively
set "SvcDelSYS=CertPropSvc DPS EFS gpsvc SCardSvr ScDeviceEnum SCPolicySvc WdiSystemHost WdiServiceHost scfilter"
set "SvcDelTI=EntAppSvc dosvc waasmedicsvc wscsvc SecurityHealthService SgrmAgent SgrmBroker windefend"
:: non-existent services, not present in current 22h2 Core release
set "SvcDelOld=AppVClient AssignedAccessManagerSvc CscService DcpSvc fax HomeGroupListener HomeGroupProvider HvHost irmon kbldfltr MixedRealityOpenXRSvc MsSecFlt NetTcpPortSharing NfsClnt OneSyncSvc PeerDistSvc RSoPProv sacsvr Sense tiledatamodelsvc timebroker UevAgentService WdBoot WdFilter WdNisDrv WdNisSvc WMPNetworkSvc wpcfltr"
:: Services which need a permission change to delete
set "SvcDelPRI=BDESVC smsrouter ngcsvc ngcctnrsvc PrintWorkflowUserSvc TimeBrokerSvc"
:: Filter Drivers
set "SvcDelFLT=fvevol rdyboost volsnap"

:: Breaking Changes
:: Licensing
set "SvcDelTI=%SvcDelTI% sppsvc clipsvc"
:: Fancy Network Features
set "SvcDelete=%SvcDelete% lltdio lltdsvc MsLldp rspndr tcpipreg mskssrv MSiSCSI mspclock qwave qwavedrv"
:: Remote Access (RAS)
set "SvcDelete=%SvcDelete% RasAcd RasAuto RasMan raspppoe NdisTapi"
:: TODO Remove Windows Firewall after removing components with w6rt (otherwise the changes fail and will be rolled back on reboot)
::set "SvcDelPRI=%SvcDelPRI% mpssvc mpsdrv"
:: Mobile Devices
::set "SvcDelete=%SvcDelete% msgpiowin32 hidbatt cmbatt"
:: Bluetooth (DevicesFlowUserSvc breaks Devices Tab in SystemSettings)
::set "SvcDelete=%SvcDelete% DevicesFlowUserSvc NcbService bthserv BthLEEnum BthEnum BthA2dp BthHFEnum BthMini BluetoothUserService RFCOMM HidBth BTHUSB BTHPORT BTHMODEM"
:: Peripherals / Vendor-specific components
set "SvcDelete=%SvcDelete% wacompen cht4iscsi cht4vbd ajrouter ibbus smartsamd EhStorClass EhStorTcgDrv"
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Class\{4d36e967-e325-11ce-bfc1-08002be10318}" /v "LowerFilters" /t REG_MULTI_SZ /d "" /f
:: Filter Drivers
set "SvcDelFLT=%SvcDelFLT% SpatialGraphFilter ksthunk"
:: Experimental
set "SvcDisable=%SvcDisable% mssmbios ndisimplatform ndisuio ndisvirtualbus netbios usbcir bam dam vacsvc"

echo Getting all services starting with vmic...
for /f "usebackq" %%v in (`PowerShell -NoP -C "(get-service | where {$_.name -like 'vmic*'}).Name"`) do (echo | set /p="%%v " >>svcvmic.txt)
set /p SvcDelVMIC=<.\svcvmic.txt && del svcvmic.txt

:: Creating a list of service / driver files to delete
:: Querying the ServiceDll value from registry, usebackq functions like command-substitution, tokens=3 gets the third column, expanding the environment variables from the REG_EXPAND_SZ using 'call'
echo building list of files belonging to removed services ...
set "SVCLIST=%Systemdrive%\svcdlls.txt"
for %%s in (%SvcDelete% %SvcDelSYS% %SvcDelTI% %SvcDelOld% %SvcDelPRI% %SvcDelFLT% %SvcDelVMIC%) do (
  if exist "%WinDir%\System32\drivers\%%s.sys" ( echo %WinDir%\System32\drivers\%%s.sys >>%SVCLIST% )
  if exist "%WinDir%\System32\%%s.dll" ( echo %WinDir%\System32\%%s.dll >>%SVCLIST% )
  if exist "%WinDir%\SysWoW64\%%s.dll" ( echo %WinDir%\SysWoW64\%%s.dll >>%SVCLIST% )
  for /f "usebackq tokens=3" %%p in (`reg query "HKLM\SYSTEM\CurrentControlSet\Services\%%s\Parameters" /v "ServiceDll" 2^>nul`) do ( call echo %%p >>%SVCLIST% )
  call echo %%s>>%Systemdrive%\svcterms.txt
)

@set sdi=0 & @set sde=0
for %%s in (%SvcDisable%) do (echo | set /p="disabling %%s: " & sc config "%%s" start= disabled && set /a sdi+=1 || set /a sde+=1 )
echo. Disabled %sdi% Services, %sde% Errors & @set "sdi=0" & @set "sde=0"

for %%s in (%SvcDelete% %SvcDelVMIC%) do (echo | set /p="deleting %%s: " & sc delete "%%s" && set /a sdi+=1 || set /a sde+=1 )
echo. Deleted %sdi% Services, %sde% Errors & @set "sdi=0" & @set "sde=0"

for %%s in (%SvcDelOld%) do (echo | set /p="(deprecated) %%s: " & sc delete "%%s" 2>nul && set /a sdi+=1 || set /a sde+=1 )
echo. Deleted %sdi% deprecated Services, %sde% Errors & @set "sdi=" & @set "sde="

echo. Removing Services of System and TrustedInstaller...
for %%s in (%SvcDelSYS%) do (echo | set /p="unlocking %%s: " & .\util\nsudolc.exe -U:S -wait -UseCurrentConsole sc delete "%%s")
for %%s in (%SvcDelTI%) do (echo | set /p="deleting %%s: " & .\util\nsudolc.exe -U:T -wait -UseCurrentConsole sc delete "%%s")

:: Neither SYSTEM nor TrustedInstaller can delete the following Services due to missing permissions,
:: Replacing Security Descriptors (acquired using 'sc sdshow') and deleting Services
:: (A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;SY) is 'Full Control' for SYSTEM (S-1-5-18)
echo. Deleting protected Services
for %%s in (%SvcDelPRI%) do (
  echo | set /p="unlocking %%s: " & .\util\nsudolc.exe -U:S -wait -UseCurrentConsole sc sdset "%%s" "D:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;SY)"
  echo | set /p="deleting  %%s: " & .\util\nsudolc.exe -U:S -wait -UseCurrentConsole sc delete "%%s"
)

for %%s in (%SvcDelFLT%) do (
  for /f "usebackq" %%i in (`reg query HKLM\SYSTEM\CurrentControlSet\Control\Class /s /t REG_MULTI_SZ /f %%s ^| findstr "HKEY"`) do (
    .\util\nsudolc.exe -U:S -wait -UseCurrentConsole reg delete "%%i" /f
  )
  echo | set /p="Deleting filter driver %%s: " & .\util\nsudolc.exe -U:S -wait -UseCurrentConsole sc delete "%%s"
)

@set SvcDelete= &@set SvcDelFLT= &@set SvcDelOld= &@set SvcDelPRI= &@set SvcDelSYS= &@set SvcDelTI= &@set SvcDisable= &@set SvcVMIC=
timeout /t 1 /nobreak
:postprisvc


:: Disabling SMBv1, effectively mitigates EternalBlue, popularly known as WannaCry.
PowerShell -NoP -C "Set-SmbServerConfiguration -EnableSMB1Protocol $false -Force"
if exist "%windir%\system32\DRIVERS\mrxsmb10.sys" (
  echo Disabling SMBv1
  sc config lanmanworkstation depend= bowser/mrxsmb20/nsi
  sc delete mrxsmb10
  del  "%windir%\system32\DRIVERS\mrxsmb10.sys"
) else (
  echo SMBv1 not present
)

:: TODO
:: https://learn.microsoft.com/en-us/windows/win32/shell/knownfolderid
:: https://learn.microsoft.com/en-us/dotnet/desktop/winforms/controls/known-folder-guids-for-file-dialog-custom-places?view=netframeworkdesktop-4.8
:: {8939299f-2315-4c5c-9b91-abb86aa0627d} (Microsoft-Windows-KnownFolders) warns about every missing folder in the event log.
echo   :: Cleaning up 'This PC'
for %%a in ( SOFTWARE SOFTWARE\Wow6432Node ) do (
  for %%b in ( MyComputer Desktop ) do (
  :: 3D Objects
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}" /f
  :: Desktop
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}" /f
  :: Documents
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{A8CDFF1C-4878-43be-B5FD-F8091C1C60D0}" /f
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{d3162b92-9365-467a-956b-92703aca08af}" /f
  :: Downloads
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{374DE290-123F-4565-9164-39C4925E467B}" /f
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{088e3905-0323-4b02-9826-5d99428e115f}" /f
  :: Music
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{1CF1260C-4DD0-4ebb-811F-33C572699FDE}" /f
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{3dfdf296-dbec-4fb4-81d1-6a3438bcf4de}" /f
  :: Pictures
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{3ADD1653-EB32-4cb0-BBD7-DFA0ABB5ACCA}" /f
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{24ad3ad4-a569-4530-98e1-ab02f9417aa8}" /f
  :: Videos
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{A0953C92-50DC-43bf-BE83-3742FED03C9C}" /f
  reg delete "HKLM\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b\NameSpace\{f86fa3ab-70d2-4fc7-9c99-fcbf05467f3a}" /f

  reg delete "HKCU\%%a\Microsoft\Windows\CurrentVersion\Explorer\%%b" /f
  )
)

echo deleted UserProfileFolders

:: Removing Entries from Explorer Side Panel
for %%a in ( CLSID Wow6432Node\CLSID ) do (
  :: OneDrive
  reg delete "HKCR\%%a\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /f
  :: Libraries
  .\util\nsudolc.exe -U:T -wait -UseCurrentConsole reg add "HKCR\%%a\{031E4825-7B94-4dc3-B131-E946B44C8DD5}\ShellFolder" /v "Attributes" /t REG_DWORD /d 0xb090010d /f
  :: Favorites
  .\util\nsudolc.exe -U:T -wait -UseCurrentConsole reg add "HKCR\%%a\{323CA680-C24D-4099-B94D-446DD2D7249E}\ShellFolder" /v "Attributes" /t REG_DWORD /d 0xa9400100 /f
:: QuickAccess
:: .\util\nsudolc.exe -U:T -wait -UseCurrentConsole reg add "HKCR\%%a\{679f85cb-0220-4080-b29b-5540cc05aab6}\ShellFolder" /v "Attributes" /t REG_DWORD /d 0xa0600000 /f
)

echo.
echo   :: Disabling Cortana and Removing Search Icon from Taskbar
taskkill /f /im SearchUI.exe 2>nul

for %%p in (HKLM HKCU HKU\.DEFAULT) do (
  reg delete "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense" /f
  echo   :: Fixing Search for %%p
  reg add "%%p\Software\Microsoft\InputPersonalization" /v "RestrictImplicitInkCollection" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\InputPersonalization" /v "RestrictImplicitTextCollection" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\InputPersonalization\TrainedDataStore" /v "AcceptedPrivacyPolicy" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\InputPersonalization\TrainedDataStore" /v "HarvestContacts" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Personalization\Settings" /v "AcceptedPrivacyPolicy" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Search" /v "AllowSearchToUseLocation" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Search" /v "BingSearchEnabled" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Search" /v "CortanaConsent" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Search" /v "CortanaInAmbientMode" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Search" /v "HasAboveLockTips" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Search" /v "HistoryViewEnabled" /t REG_DWORD /d 0 /f
  reg add "%%p\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" /v "SearchboxTaskbarMode" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\SearchSettings" /v "SafeSearchMode" /t REG_DWORD /d 0 /f
)

reg add "HKLM\SOFTWARE\Microsoft\Speech_OneCore\Preferences" /v "ModelDownloadAllowed" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Speech_OneCore\Preferences" /v "VoiceActivationDefaultOn" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Speech_OneCore\Preferences" /v "VoiceActivationEnableAboveLockscreen" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\OOBE" /v "DisableVoice" /t REG_DWORD /d 1 /f

echo   :: Start Layout
taskkill /f /im "SearchApp.exe"
Import-StartLayout -LayoutPath .\util\StartLayout.xml -MountPath "%SystemDrive%\"

echo   :: Personalization
echo   :: Setting up Explorer

:: Show Accent Color on Title Bars and Window Borders
:: Change VisualEffects settings to best performance, except 'Smooth Edges of Screen Fonts' - FontSmoothing
:: Prevent Keyboard Layout / Language toggle via hotkeys, use the taskbar icon
:: Alternatively change the Keyboard Layout Switch Hotkeys via GUI
:: rundll32 Shell32,Control_RunDLL input.dll,,{C07337D3-DB2C-4D0B-9A93-B722A6C106E2}
for %%p in (HKLM HKCU HKU\.DEFAULT) do (
  reg add "%%p\Control Panel\Desktop" /v "DragFullWindows" /t REG_DWORD /d 0 /f
  reg add "%%p\Control Panel\Desktop" /v "FontSmoothing" /t REG_SZ /d "2" /f
  reg add "%%p\Control Panel\Desktop" /v "UserPreferencesMask" /t REG_BINARY /d 9012038010000000 /f
  reg add "%%p\Control Panel\Desktop\WindowMetrics" /v "MinAnimate" /t REG_DWORD /d 0 /f
  reg add "%%p\Control Panel\International" /v "sShortDate" /t REG_SZ /d "yyyy-MM-dd" /f
  reg add "%%p\Keyboard Layout\Toggle" /v "HotKey" /t REG_DWORD /d 3 /f
  reg add "%%p\Keyboard Layout\Toggle" /v "Language HotKey" /t REG_DWORD /d 3 /f
  reg add "%%p\Keyboard Layout\Toggle" /v "Layout HotKey" /t REG_DWORD /d 3 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "DisallowShaking" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "Hidden" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "HideFileExt" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "IconsOnly" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "LaunchTo" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "ListviewAlphaSelect" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "ListviewShadow" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "NavPaneShowAllFolders" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "ServerAdminUI" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "ShowSuperHidden" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "ShowSyncProviderNotifications" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "TaskbarAnimations" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects" /v "VisualFXSetting" /t REG_DWORD /d 3 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize" /v "AppsUseLightTheme" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize" /v "EnableTransparency" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize" /v "SystemUsesLightTheme" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\DWM" /v "AlwaysHibernateThumbnails" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Microsoft\Windows\DWM" /v "ColorPrevalence" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Microsoft\Windows\DWM" /v "EnableAeroPeek" /t REG_DWORD /d 0 /f
  reg add "%%p\Software\Policies\Microsoft\Windows\CurrentVersion\PushNotifications" /v "NoTileApplicationNotification" /t REG_DWORD /d 1 /f
  reg add "%%p\Software\Policies\Microsoft\Windows\Explorer" /v "MultiTaskingAltTabFilter" /t REG_DWORD /d 4 /f
  reg add "%%p\Software\Policies\Microsoft\Windows\EdgeUI" /v "AllowEdgeSwipe" /t REG_DWORD /d 0 /f
  reg delete "%%p\Control Panel\Accessibility" /f
)

:: Remove redundant Keyboard Layouts
PowerShell -NoP -C "Set-WinUserLanguageList $((Get-WinUserLanguageList) | where LanguageTag -ne 'en-GB') -Force"

echo   :: Setting timezone
tzutil /s UTC

echo   :: Removing filetype associations
reg delete "HKCR\CABFolder\CLSID" /f
reg delete "HKCR\SystemFileAssociations\.cab\CLSID" /f
reg delete "HKCR\CompressedFolder\CLSID" /f
reg delete "HKCR\SystemFileAssociations\.zip\CLSID" /f
reg delete "HKCR\Microsoft.System.Update.1" /f
reg delete "HKCR\.msu" /f
reg delete "HKCR\.js" /f
reg delete "HKCR\.jse" /f
.\util\nsudolc.exe -U:T -wait -UseCurrentConsole reg delete "HKCR\xmlfile" /f

:: Removing 'Open with Paint 3D' from the explorer context menu
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.bmp\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.jpeg\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.jpe\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.jpg\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.png\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.gif\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.tif\Shell\3D Edit" /f
reg delete "HKLM\SOFTWARE\Classes\SystemFileAssociations\.tiff\Shell\3D Edit" /f

:: No active internet connectivity check - network status changes a bit slower
reg add "HKLM\SYSTEM\CurrentControlSet\Services\NlaSvc\Parameters\Internet" /v "EnableActiveProbing" /t REG_DWORD /d 0 /f

:: Remove Shell Extensions seen in Explorer (right click Context menu)
reg delete "HKCR\*\shellex\ContextMenuHandlers\ModernSharing" /f & :: Sharing element

:: 'Scan With Windows Defender'
reg delete "HKCR\*\shellex\ContextMenuHandlers\EPP" /f
reg delete "HKCR\drive\shellex\ContextMenuHandlers\EPP" /f
reg delete "HKCR\directory\shellex\ContextMenuHandlers\EPP" /f

:: Removes internet explorer option when setting default browser in settings
reg delete "HKLM\SOFTWARE\Microsoft\Internet Explorer" /f
reg delete "HKLM\SOFTWARE\RegisteredApplications" /v "Internet Explorer" /f
reg delete "HKLM\SOFTWARE\Clients\StartMenuInternet\IEXPLORE.EXE" /f
reg delete "HKCR\Applications\iexplore.exe" /f

echo Removing Explorer ShellExtensions...
echo   Include in Library
reg delete "HKCR\Folder\shellex\ContextMenuHandlers\Library Location" /f
echo   Pin to Start Menu
reg delete "HKCR\Folder\shellex\ContextMenuHandlers\PintoStartScreen" /f
reg delete "HKCR\exefile\shellex\ContextMenuHandlers\PintoStartScreen" /f
reg delete "HKCR\Microsoft.Website\ShellEx\ContextMenuHandlers\PintoStartScreen" /f
reg delete "HKCR\mscfile\shellex\ContextMenuHandlers\PintoStartScreen" /f
echo   Play To Menu
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Blocked" /v "{7AD84985-87B4-4a16-BE58-8B72A5B390F7}" /t REG_SZ /d "Play to Menu" /f
echo   Remove the Restore previous versions menu
reg delete "HKCR\AllFilesystemObjects\shellex\PropertySheetHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\CLSID\{450D8FBA-AD25-11D0-98A8-0800361B1103}\shellex\PropertySheetHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\Directory\shellex\PropertySheetHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\Drive\shellex\PropertySheetHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\AllFilesystemObjects\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\CLSID\{450D8FBA-AD25-11D0-98A8-0800361B1103}\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\Directory\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f
reg delete "HKCR\Drive\shellex\ContextMenuHandlers\{596AB062-B4D2-4215-9F74-E9109B0A8153}" /f

:: Restore 'Open With'
reg add "HKCR\*\shellex\ContextMenuHandlers\Open With" /d "{09799AFB-AD67-11d1-ABCD-00C04FC30936}" /f

echo   :: Removing AppxPackages
:: Unprovision built in apps, the list in this command is a whitelist (prevents black screen when logging in as a new user for the first time)
Powershell -NoP -C "Get-AppxProvisionedPackage -Online | Remove-AppxProvisionedPackage -Online"
:: Remove all packages possible (packages can be excluded with -notlike patterns )
PowerShell -NoP -ExecutionPolicy Unrestricted ".\util\appx.ps1"

:removeedge
taskkill /f /im "msedge.exe" >nul 2>&1
taskkill /f /im "MicrosoftEdgeUpdate.exe" >nul 2>&1

echo Removing Edge
pushd "%PROGRAMFILES(X86)%\Microsoft"
for /f "delims=? usebackq" %%e in (`dir /b /l /a-d /s ^| findstr /r ".*edge.*\\.*install.*\\.*\.exe$"`) do (
  echo | set /p="Found Edge at '%%e', removing. this might take some time..."
  start "" "%%e" --uninstall --system-level --force-uninstall
  if %errorlevel% equ 0 (echo success) else (echo failed)
)
popd

:components
:: Removing Components, assuming they weren't already removed during iso creation
PowerShell -NoP -ExecutionPolicy Unrestricted -File ".\util\components.ps1" -ArgumentList "-mode Online"
::del Packages.txt

echo deleting edge leftovers
for /f "usebackq" %%e in (`dir "%PROGRAMFILES(X86)%\Microsoft" /b /l /ad ^| findstr /r ".*edge.*"`) do (rd /s /q "%%e")

:preame2
echo   :: Proceeding after w6rt

DISM /Online /Set-ReservedStorageState /State:Disabled
PowerShell -NoP -C "Disable-MMAgent -MemoryCompression"

:: Disabling One Drive
echo.
echo   :: Uninstalling OneDrive

set "onedrvx64=%SYSTEMROOT%\SysWOW64\OneDriveSetup.exe"
taskkill /f /im OneDrive.exe

if exist "%onedrvx64%" (
  "%onedrvx64%" /uninstall
) else (
  echo OneDriveSetup.exe installer not found, skipping.
)

:: Editing Hosts File (fallback), unreliable
echo.
echo  :: Editing Hosts File
set nl=^& echo.
set hostsfile=%SYSTEMROOT%\System32\drivers\etc\hosts
set "domains=a-0001.a-msedge.net activation.sls.microsoft.com cdn.content.prod.cms.msn.com.edgekey.net cdn.content.prod.cms.msn.com choice.microsoft.com.nsatc.net choice.microsoft.com compatexchange.cloudapp.net corp.sts.microsoft.com corpext.msitadfs.glbdns2.microsoft.com cs1.wpc.v0cdn.net df.telemetry.microsoft.com diagnostics.support.microsoft.com dmd.metaservices.microsoft.com e10663.g.akamaiedge.net fe2.update.microsoft.com.akadns.net feedback.microsoft-hohm.com feedback.search.microsoft.com feedback.windows.com go.microsoft.com i1.services.social.microsoft.com.nsatc.net i1.services.social.microsoft.com oca.telemetry.microsoft.com.nsatc.net oca.telemetry.microsoft.com pre.footprintpredict.com redir.metaservices.microsoft.com reports.wes.df.telemetry.microsoft.com schemas.microsoft.com services.wes.df.telemetry.microsoft.com settings-sandbox.data.microsoft.com sls.update.microsoft.com.akadns.net sqm.df.telemetry.microsoft.com sqm.telemetry.microsoft.com.nsatc.net sqm.telemetry.microsoft.com statsfe1.ws.microsoft.com statsfe2.update.microsoft.com.akadns.net statsfe2.ws.microsoft.com survey.watson.microsoft.com telecommand.telemetry.microsoft.com.nsatc.net telecommand.telemetry.microsoft.com telemetry.appex.bing.net telemetry.microsoft.com telemetry.urs.microsoft.com vortex-sandbox.data.microsoft.com vortex-win.data.microsoft.com vortex.data.microsoft.com watson.live.com watson.microsoft.com watson.ppe.telemetry.microsoft.com watson.telemetry.microsoft.com.nsatc.net watson.telemetry.microsoft.com wes.df.telemetry.microsoft.com"
for %%s in ( %domains% ) do (
  find /c /i "%%s" %hostsfile% & if %ERRORLEVEL% neq 0 @echo %nl%^0.0.0.0 %%s>>%hostsfile%
)

echo setting boot manager configuration
:: Enable Legacy F8 Bootmenu, allows access to safemode
bcdedit /set {default} bootmenupolicy legacy
bcdedit /set {current} recoveryenabled no
:: Disable Hibernation to make NTFS accessible outside of Windows
powercfg /h off
:: Performance Plan - High Performance
powercfg /S 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
:: Set Monitor timeout in minutes
powercfg /change monitor-timeout-ac 120

:: Setting NTP (Time) servers
w32tm /config /syncfromflags:manual /manualpeerlist:"0.pool.ntp.org 1.pool.ntp.org 2.pool.ntp.org 3.pool.ntp.org"

:: NTFS Settings
fsutil behavior set DisableLastAccess 1
fsutil behavior set DisableEncryption 1
fsutil behavior set DisableDeleteNotify 0
:: may break compatibility with very old programs depending on 8.3 file naming
fsutil behavior set AllowExtChar 0
fsutil behavior set Disable8Dot3 1

:: Clear PageFile at shutdown, causes long shutdown time
:: reg add "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v "ClearPageFileAtShutdown" /t REG_DWORD /d 1 /f

set /P nopagefile= Do you want to disable the pagefile [y/N]?
if "%nopagefile%"=="y" (
  wmic computersystem where name="%computername%" set AutomaticManagedPagefile=False
  wmic pagefileset delete
)
echo.
set /P hostchange=enter new hostname. leave empty to keep %computername%:
if not "%hostchange%"=="" (
  echo new name is "%hostchange%"
  wmic computersystem where name="%computername%" call rename name="%hostchange%"
)

goto reboot

:postame

:firewall
echo   :: Configuring Firewall...
PowerShell -NoP -ExecutionPolicy Unrestricted -File ".\util\firewall\firewall.ps1"

echo   :: Removing unlocked AppxPackages
:: Remove the remaining AppxPackages and schtasks,
PowerShell -NoP -ExecutionPolicy Unrestricted ".\util\appx.ps1"
schtasks /delete /TN "\Microsoft\Windows\WindowsUpdate\Scheduled Start" /f
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Cache Maintenance" /f 2>nul
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Cleanup" /f 2>nul
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Scheduled Scan" /f 2>nul
schtasks /delete /TN "\Microsoft\Windows\Windows Defender\Windows Defender Verification" /f 2>nul

:: hide "allow PC to be discoverable on this network" popup
reg add "HKLM\System\CurrentControlSet\Control\Network\NewNetworkWindowOff" /f

:: UAC admin consent behaviour - always show password input field
:: reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "ConsentPromptBehaviorAdmin" /t REG_DWORD /d 3 /f

:: Repurpose the Organization field as ame version identifier
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v "RegisteredOrganization" /t REG_SZ /d "%ameversion%" /f

:: Remove UWP / Appx / Store Services after the AppxPackages have been removed
:: beware: this also prevents the installation of AppxPackages (uwp apps)

:: parts of the UWP infrastructure
sc delete LicenseManager
sc delete camsvc
:: AppxSvc is needed for Get-AppxPackage, etc. but can be deleted if AppxPackages will never be changed again
sc stop appxsvc
.\util\nsudolc.exe -U:T -wait -UseCurrentConsole sc config AppxSvc start= disabled

:chk_network
echo  :: Checking For Internet Connection...
ping -n 1 archlinux.org -w 20000 >nul
if %errorlevel% equ 0 (
  echo Internet Connection found! Proceeding...
  goto prechoco
) else (
  schtasks /run /tn "netadpt1"
  echo   :: You are not connected to the Internet
  echo    Enable and connect your Networking adapter and try again
  echo    Press any key to retry...
  pause >nul
  goto chk_network
)
:: disabling router discovery
for /f "tokens=1" %%a in ('netsh int ip show interfaces ^| findstr [0-9]') do (
  netsh int ip set interface %%a routerdiscovery=disabled store=persistent
)

:prechoco
echo  :: Installing Packages...
:: installing a package-manager 'chocolately'
set PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
PowerShell -NoP -ExecutionPolicy Unrestricted -C "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))"
if %ERRORLEVEL% neq 0 (
  echo failed to install chocolately
  exit
) else ( goto instpkg )

:instpkg
PowerShell -NoP -ExecutionPolicy Unrestricted -C Start-Process "${env:ProgramFiles}\simplewall\simplewall.exe" -Verb RunAs
PowerShell -NoP -ExecutionPolicy Unrestricted -C "choco install test 2>$null"
echo Allow network access for choco.exe in simplewall
set /P firewallenabled=Done [y/N]?
if "%firewallenabled%"=="y" (
  echo choco unblocked, proceeding
) else ( goto instpkg )

PowerShell -NoP -ExecutionPolicy Unrestricted -C "choco install -y open-shell --params='/StartMenu'"
PowerShell -NoP -ExecutionPolicy Unrestricted -C "choco install -y 7zip --params='/NoDesktopShortcut'"

goto postchoco

:postchoco
set /P vc_install=Do you want to install the VC Redistributables ? ([l]atest / [a]ll / [N]o):
if "%vc_install%"=="l" (
  echo fetching vcredist17x64...
  PowerShell -NoP -C "Invoke-WebRequest -uri 'https://aka.ms/vs/17/release/vc_redist.x64.exe' -outfile .\vcredistx64.exe"
  start "" ".\vcredistx64.exe" /install /quiet /norestart
  echo fetching vcredist17x86...
  PowerShell -NoP -C "Invoke-WebRequest -uri 'https://aka.ms/vs/17/release/vc_redist.x86.exe' -outfile .\vcredistx86.exe"
  start "" ".\vcredistx86.exe" /install /quiet /norestart
)
if "%vc_install%"=="a" (
  PowerShell -NoP -ExecutionPolicy Unrestricted -C "choco install -y vcredist-all"
)

set /P dx_install=Do you want to install DirectX [y/N] ?
if "%dx_install%"=="y" (
  echo downloading DirectX End-User Runtime...
  PowerShell -NoP -C "Invoke-WebRequest -uri 'https://download.microsoft.com/download/1/7/1/1718CCC4-6315-4D8E-9543-8E28A4E18C4C/dxwebsetup.exe' -outfile .\dxwebsetup.exe"
  "%ProgramFiles%\7-Zip\7z.exe" x .\dxwebsetup.exe -o.\dxwebsetup -y
  start "" ".\dxwebsetup\dxwsetup.exe"
)

set /P si_install=Do you want to install the Sysinternals Suite [y/N] ?
if "%si_install%"=="y" (
  echo downloading...
  PowerShell -NoP -C "Invoke-WebRequest -uri 'https://download.sysinternals.com/files/SysinternalsSuite.zip' -outfile .\si.zip"
  "%ProgramFiles%\7-Zip\7z.exe" x .\si.zip -o.\sysinternals -y
  :: also add the Microsoft Error Lookup Tool
  PowerShell -NoP -C "Invoke-WebRequest -uri 'https://download.microsoft.com/download/4/3/2/432140e8-fb6c-4145-8192-25242838c542/Err_6.4.5/Err_6.4.5.exe' -outfile .\sysinternals\err.exe"
  PowerShell -NoP -C "if ((Get-FileHash .\err.exe -Algorithm SHA256).hash -ne '88739EC82BA16A0B4A3C83C1DD2FCA6336AD8E2A1E5F1238C085B1E86AB8834A') { rm .\sysinternals\err.exe }"
  setx path "%PATH%;%PROGRAMFILES%\sysinternals" /M
  del .\si.zip
)
echo.
echo post-ame stage done - install 3rd-party software and continue with stage 4
goto menu

:user
:: Open User preferences to configure administrator/user permissions
:: Configuration of installed software is considered done by now.
echo   :: Manual User Permission Adjustment

:: Nvidia Container
sc delete NVDisplay.ContainerLocalSystem /f 2>nul
reg delete "HKCR\DesktopBackground\Shell\NVIDIAContainer" /f 2>nul

:: Remove "AMD Radeon Software" Shell Extension (Context Menu)
reg delete "HKLM\SOFTWARE\Classes\Directory\background\shellex\ContextMenuHandlers\ACE" /f 2>nul

:: enables Secure sign-in (Require users to press Ctrl+Alt+Delete on logon)
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v "DisableCAD" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v "EnableFirstLogonAnimation" /t REG_DWORD /d 0 /f

:: add a "Take Ownership Shell Extension" (https://github.com/meetrevision/playbook/blob/main/src/Configuration/features/revision/registry/add-take-ownership-in-context-menu.yml)
reg add "HKCR\*\shell\runas" /v "" /t REG_SZ /d "Take Ownership"
reg add "HKCR\*\shell\runas" /v "HasLUAShield" /t REG_SZ /d ""
reg add "HKCR\*\shell\runas" /v "NoWorkingDirectory" /t REG_SZ /d ""
reg add "HKCR\*\shell\runas" /v "Position" /t REG_SZ /d "middle"
reg add "HKCR\*\shell\runas\command" /v "" /t REG_SZ /d "cmd.exe /c takeown /f "%1" && icacls "%1" /grant *S-1-3-4:F /c /l"
reg add "HKCR\*\shell\runas\command" /v "IsolatedCommand" /t REG_SZ /d "cmd.exe /c takeown /f "%1" && icacls "%1" /grant *S-1-3-4:F /c /l"
reg add "HKCR\Directory\shell\runas" /v "" /t REG_SZ /d "Take Ownership"
reg add "HKCR\Directory\shell\runas" /v "HasLUAShield" /t REG_SZ /d ""
reg add "HKCR\Directory\shell\runas" /v "NoWorkingDirectory" /t REG_SZ /d ""
reg add "HKCR\Directory\shell\runas" /v "Position" /t REG_SZ /d "middle"
reg add "HKCR\Directory\shell\runas\command" /v "" /t REG_SZ /d "cmd.exe /c takeown /f "%1" /r /d y && icacls "%1" /grant *S-1-3-4:F /t /c /l /q"
reg add "HKCR\Directory\shell\runas\command" /v "IsolatedCommand" /t REG_SZ /d "cmd.exe /c takeown /f "%1" /r /d y && icacls "%1" /grant *S-1-3-4:F /t /c /l /q"
reg add "HKCR\dllfile\shell\runas" /v "" /t REG_SZ /d "Take Ownership"
reg add "HKCR\dllfile\shell\runas" /v "HasLUAShield" /t REG_SZ /d ""
reg add "HKCR\dllfile\shell\runas" /v "NoWorkingDirectory" /t REG_SZ /d ""
reg add "HKCR\dllfile\shell\runas" /v "Position" /t REG_SZ /d "middle"
reg add "HKCR\dllfile\shell\runas\command" /v "" /t REG_SZ /d "cmd.exe /c takeown /f "%1" && icacls "%1" /grant *S-1-3-4:F /c /l"
reg add "HKCR\dllfile\shell\runas\command" /v "IsolatedCommand" /t REG_SZ /d "cmd.exe /c takeown /f "%1" && icacls "%1" /grant *S-1-3-4:F /c /l"
reg add "HKCR\exefile\shell\runas" /v "HasLUAShield" /t REG_SZ /d ""
reg add "HKCR\exefile\shell\runas\command" /v "" /t REG_SZ /d ""%1" %*"
reg add "HKCR\exefile\shell\runas\command" /v "IsolatedCommand" /t REG_SZ /d ""%1" %*"

net user administrator /active:yes
echo set a administrator password and set user account type to standard
:: https://www.stigviewer.com/stig/windows_10/2021-08-18/finding/V-220712
netplwiz
goto menu

:reboot
echo.
echo   :: A reboot is required to complete setup.
echo   Press any key to reboot
pause >nul
shutdown -r -t 1 -f
