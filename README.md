# Windows Ameliorated Extended

Windows out of the box is no usable OS. This project is a collection of scripts to shorten the path from a new installation to a usable system.

This project is a fork of [AMEliorated](https://ameliorated.info/), targeted on a more aggressive removal of unwanted features and components.  
Many procedures have been automated, like the updating and modification of the ISO file, or configuring the environment.

These Scripts could remove or break features that you might want, so read and edit the scripts before running, e.g. by commenting passages of code out.

# Usage
## Building a cleaned OS-Image (optional)
This part is optional but helps to save time and storage space when deploying on more than one machine.
It can be further customized, see [Windows ADK](https://learn.microsoft.com/en-us/windows-hardware/get-started/).

1. Install the [Windows ADK](https://learn.microsoft.com/en-us/windows-hardware/get-started/).
Only the Feature `Deployment Tools` is needed, it contains tools to build an os-image.

2. Download the OS ISO from Microsoft, e.g. [from the web](https://www.microsoft.com/en-us/software-download/windows10ISO) or third-party-sources like [rg-adguard](https://tb.rg-adguard.net/public.php)

3. Download the latest Cumulative Update from the [Microsoft Update Catalog](https://www.catalog.update.microsoft.com/Home.aspx).
To get a unique result, specify OS Architecture and Version ([Example](https://www.catalog.update.microsoft.com/Search.aspx?q=Windows%2010%2022H2%20x64%20Cumulative)).
4. Unpack the downloaded `.msu`-file and extract the biggest `.cab` file which contains the actual update. Place the `.cab` file in the [build/updates](build/updates) directory, all Cabinet-files in this folder will get applied to the original ISO.
Downloading a ServicingStackUpdate (SSU) in addition to the CSU is not required, as the SSU is already contained in the CSU.

5. Open and edit the [build_iso.ps1](ame/build_iso.ps1), verify that the paths defined at the top are correct.

6. Run [build_iso.ps1](ame/build_iso.ps1) with elevated Permissions (Administrator). One can also place breakpoints and inspect the results after each step (use Powershell ISE or VSCode for that - run them elevated as well.)

An iso (`IsoOut`) is generated.
### Background
The resulting image (`IsoOut`) contains the latest updates and is stripped of most WindowsOptionalFeatures, WindowsCapabilities and AppxProvisionedPackages.
The filter lists can be easily edited in the [build_iso.ps1](ame/build_iso.ps1) file.

## Installation
During the whole installation process, the Machine must be kept **offline**.

Boot with the generated iso and follow the Installer. Once it reboots select Language, Username, etc. And
proceed with `I don't have Internet`. Refuse all "Features" e.g. Location Tracking, Ad Tracking, etc.

## AME Part 1
Once on a desktop, navigate to the `ame` folder and run [ame.bat](ame/ame.bat).

Install Dotnet3.5 if desired, proceed with entering `1`.
Once done, AME needs the system to reboot into a Linux OS, this can be a BootStick / ISO / DualBoot.
Make sure to reboot into Linux **immediately**, Windows should not restart before AME Part 2 is completed. Otherwise, Windows may try to undo some changes.

### Background
This section changes Registry Entries to configure windows in an automated way, e.g. enables all privacy-related options, disables telemetry, cortana, advertisements ...

It also disables and deletes many [Services](doc/services.csv) and Scheduled Tasks, along with setting registry keys to save the user a few hours of manual configuration.

The changes are commented throughout the code so having a look at [ame.bat](ame/ame.bat) should give a good overview.

## AME Part 2 - Amelioration

Once booted into Linux, open a Shell and mount the Windows partition.

Use `lsblk -f` to list partitions.
An example to mount: `mount /dev/sda3 /mnt`

Run [ame.sh](ame/ame.sh) on the mounted partition with the desired parameters. Call `ame.sh` without parameters to get a usage hint.

Example: `[lx@linux /mnt]$ Users/myuser/Documents/ame/ame.sh`
```
usage: [sudo] ame.sh -p <windows partition> [-b]
-b: make a backup of the files to delete
-p: windows partition's path or device
-k: don't archive the index file
-s: keep files in WinSxS directory
-h: show this usage message and quit
example: sudo ame.sh -p /dev/sda3 -b

[user@linux /mnt]$ Users/myuser/Documents/ame/ame.sh -p /dev/sda3 -b
```
Once `ame.sh` is finished, reboot into Windows.

It is also possible to run `ame.sh` locally, i.e. without mounting the windows partition, the script will mount it then.
### Background
[ame.sh](ame/ame.sh) is a bash-script which essentially removes files matching a specified pattern from the Windows drive.
The script will search for and delete files containing blacklisted terms. The terms themselves can be found in [util/ame_terms.txt](ame/util/ame_terms.txt).

This is done from Linux, because some files can't be modified on a running Windows even with `SYSTEM` or `TrustedInstaller` permissions.
On Linux there are no such restrictions.

The unwanted files are being backed at the root of the windows drive, so that they can be restored (e.g. to install an update with dism (and rerun Parts 1 - 3 afterwards)).

## AME Part 3 - Post ame, Installation of Programs
Again, call [ame.bat](ame/ame.bat), enter `3`.

At this stage, more AppxPackages can be removed because they were unlocked for removal by `ame.sh`.

AME will check for an internet connection, now connect the machine to the internet.
The script will open the simplewall and ask if the PacketManager Choco is allowed to access the internet.
The checkbox in simplewall should already be checked.
This section can be edited to install arbitrary software via Choco or other tools.

By default `ame.bat` installs 7z, OpenShell, Chocolately and asks whether to install the [SysInternalsSuite](https://learn.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite) and VCRedist.

Now, install applications and configure everything else, when done, proceed to Part 4.

## AME Part 4 - Lowering User Permissions (optional)
This Part completes the installation procedure.
The `Administrator` User gets activated, and the `netplwiz` opens.
You should assign a password to `Administrator` and lower the permissions of your user account to `Standard User`.

## Thanks to
- [AMEliorated](https://ameliorated.info/)
- [w6rt](https://github.com/shiitake/win6x_registry_tweak)
- [NSudo](https://github.com/M2Team/NSudo)
- [simplewall](https://github.com/henrypp/simplewall)
