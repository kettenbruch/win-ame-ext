# Terminology

## Components
Elements of Windows Experience. Components can be programs, services and tasks, capabilities, (optional) features, bugs, tools, etc.
Wikipedia has a [List of Windows Components](https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_components)
They should not be confused with COMs (Component Object Model).

## "Online" and "Offline" Images
These terms have nothing to do with the internet or even networking in general. They are likely to be encountered when working with Images, Registry Hives, Installations, etc.
`Online` means that the object is part of the currently running OS, while `Offline` means that it's not.
For example: `dism /online /...` instructs `dism` to perform operations on the currently running ("hot") System, and `dism /Image:"C:\test-01" /...` will perform operations in the offline-image mounted at `C:\test-01`.
Or when mounting an offline registry hive with `reg load hklm\test-01-sys Windows/System32/config/SYSTEM` to modify it while it's "offline" from its OS.

# Windows Edition
[build_iso.ps1](../ame/build_iso.ps1) uses the `Core` (Home) Edition by default, because it includes everything needed and is the least bloated Edition.

Some Applications require the Media Feature Pack (Errors like `mfplat.dll not found`), since 21h1 the MediaFeaturePack cannot be installed as no packages are provided, therefore an option is added to choose between the `CoreN` and `Core` versions. The default is `Core` because the Media Feature Pack cannot be easily reinstalled.  

Other Editions include special Features which are rarely needed by the average user.
Refer to this [Comparison chart](https://en.wikipedia.org/wiki/Windows_10_editions).

The Edition [build_iso.ps1](../ame/build_iso.ps1) uses can be easily changed by looking up the index of the desired Edition (`dism /get-wiminfo /Wimfile:<path to wimfile>`) and skipping the removal of **that particular** index in the for-loop.

A more lightweight solution might be to copy the needed dlls from a System with Media Feature Pack. (About 20 dlls in System32). 

# Multi-User Installations:
The user created during initial setup is considered the final user.
When other users are needed, create them after running [ame.bat](../ame/ame.bat) sections 1-3.

The newly created users will inherit settings from the `default user` which have been adjusted in the first run.
So, while not extensively tested, multi-user installations should work just fine.
Maybe some AppxPackages get installed for the other users in which case running [ame.bat](../ame/ame.bat) on the new-user's account should solve this.

# Services
Many services use cryptic names from which it's hard to deduce their meaning. Therefore, a table [services.csv](services.csv) has been created as an attempt to document their purpose.

# WinInit Logs
`autochk` sometimes performs checks of storage devices on boot, it doesn't log to the EventLog itself - `Wininit` does, however. The entry includes the full output of `autochk`. 

# Removing Filter Drivers
The naive removal of some Filter drivers (such as `fvevol` or `volsnap`) may lead to a Bug check (Bluescreen) - such as `INACCESSIBLE_BOOT_DEVICE`.

## Resolution
Filter drivers depend on each other, if one driver depends on a nonexistent driver, the system refuses to boot.
These dependencies need to be removed in the Registry when disabling a filter driver.

Information about Filter Drivers are stored under `HKLM\SYSTEM\CurrentControlSet\Control\Class\x` where x is a GUID. 
The Service's (Driver's) Name is used to specify dependencies in `REG_MULTI_SZ` Values named `UpperFilters` and `LowerFilters`. Removing the name from those Values suffices to have a bootable system without the filter-driver.

A more radical approach is searching in `HKLM\SYSTEM\CurrentControlSet\Control\Class\` and deleting all keys containing a filter driver's name:
```shell
set "driverToSearch=fvevol"
for /f "usebackq" %i in (`reg query HKLM\SYSTEM\CurrentControlSet\Control\Class /s /t REG_MULTI_SZ /f %driverToSearch% ^| findstr "HKEY_LOCAL_MACHINE"`) do reg delete "%i" /f
```

or remove all references to the driver with PowerShell:
```shell
$driverToSearch="ehstorclass"
$filters = $(ls "HKLM:\SYSTEM\CurrentControlSet\Control\Class" | where {$_.GetValueNames() -contains "UpperFilters" -or $_.GetValueNames() -contains "LowerFilters"} )
$l = $filters | where {$_.GetValue("LowerFilters") -contains $driverToSearch}
$u = $filters | where {$_.GetValue("UpperFilters") -contains $driverToSearch}
foreach ($i in $l) { [string[]]$j = $i.GetValue("LowerFilters") -notmatch $driverToSearch;  $i.SetValue("LowerFilters", $j, [Microsoft.Win32.RegistryValueKind]::MultiString) }
foreach ($i in $u) { [string[]]$j = $i.GetValue("UpperFilters") -notmatch $driverToSearch;  $i.SetValue("UpperFilters", $j, [Microsoft.Win32.RegistryValueKind]::MultiString) }
```

# Network Leaks
A mechanism to [prevent network leaks at startup](https://superuser.com/questions/1555206/how-to-block-network-leaks-during-boot-time-on-windows) has also been implemented.
Together with [simplewall](https://github.com/henrypp/simplewall) (a frontend to the Win Filtering Platform) one has quite tight and user-friendly control over what applications and services are allowed to communicate via the internet.

# Needed files and folders
[ame.sh](../ame/ame.sh) generates an index `ame_idx.txt` of all files in the Windows drive using `find`.
Then `grep` is used to find regular expressions "terms" from [util/ame_terms.txt](../ame/util/ame_terms.txt) which match a path in `ame_idx.txt`.

Most files which got found can be deleted, yet a few are **needed** ones which are whitelisted:
- `FileMaps` (from original AME)
- `MSRAW` MS RAW Image format helper, prefix of "msra"
- `Telemetry.Common.dll` Systemsettings.exe depends on it (errors like "cannot find `ms-settings:...` association".)
- `Windows.Gaming.Input.dll` UWP Gaming Input Interface, some Games depend on it
- `xboxgip.sys` Xbox Game Input Driver, needed for Xbox HID Hardware
- 
# Component Store (WinSxS)
WinSxS is the component Store which holds current and old versions of most system related files.
sfc "repairs" files by coping (hard-linking) them from WinSxS to their intended location.
Deleting files in WinSxS shouldn't cause problems granted that deleting the files outside is fine. 
Files in sxs are not explicitly used for normal operation, only for updates (patching) or when verifying/repairing the system, e.g. with `sfc /scannow` or `sfc /checkhealth` or `scf /restorehealth`.
The Integrity of WinSxS gets checked, and it will get flagged as corrupted when deleting files in there.

Therefore: Include WinSxS is in `neededPaths` when planning to update the OS later on. Otherwise, let `ame.sh` delete files in there, which helps to prevent windows from restoring unwanted files.

On a regularly updated Windows System the Component Store easily reaches sizes above 5 GiB, especially when many updates accumulate. One can use dism: `dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase` to compact the SxS. 

# AppxPackage Names are GUIDs 
Resolve them using a regular expression, case-insensitive, matching against the GUID (optional curly brackets):

`get-appxpackage |where {$_.name -match '(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}'} |select name,installlocation`

Powershell returns the Guids and Install Locations which have human-readable names.

```
Name                                        InstallLocation
----                                        ---------------
F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE        C:\Windows\SystemApps\Microsoft.Windows.AddSuggestedFoldersToLibraryDialog_cw5n1h2txyewy
E2A4F912-2574-4A75-9BB0-0D023378592B        C:\Windows\SystemApps\Microsoft.Windows.AppResolverUX_cw5n1h2txyewy
c5e2524a-ea46-4f67-841f-6a9465d9d515        C:\Windows\SystemApps\Microsoft.Windows.FileExplorer_cw5n1h2txyewy
1527c705-839a-4832-9118-54d4Bd6a0c89        C:\Windows\SystemApps\Microsoft.Windows.FilePicker_cw5n1h2txyewy
```

# Clearing all Event Log Entries
`wevtutil el | Foreach-Object {wevtutil cl "$_"}`



